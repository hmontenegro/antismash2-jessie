# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2010-2012 Marnix H. Medema
# University of Groningen
# Department of Microbial Physiology / Groningen Bioinformatics Centre
#
# Copyright (C) 2011,2012 Kai Blin
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

from antismash import utils

def convert_records(seq_records, options):
    records = []
    annotations = load_cog_annotations()
    for srec in seq_records:
        records.append(convert_record(srec, annotations, options))
    return records

def convert_record(record, annotations, options):
    """Convert a SeqRecord to JSON"""
    js_rec = {}
    js_rec['seq_id'] = record.id

    js_rec['clusters'] = convert_clusters(record, annotations, options)

    return js_rec

def convert_clusters(record, annotations, options):
    """Convert cluster SeqFeatures to JSON"""
    js_clusters = []
    for cluster in utils.get_cluster_features(record):
        features = utils.get_cluster_cds_features(cluster, record)

        js_cluster = {}
        js_cluster['start'] = int(cluster.location.start) + 1
        js_cluster['end'] = int(cluster.location.end)
        js_cluster['idx'] = utils.get_cluster_number(cluster)
        js_cluster['orfs'] = convert_cds_features(record, features, annotations, options)
        js_cluster['type'] = utils.get_cluster_type(cluster)
        if 'probability' in cluster.qualifiers:
            js_cluster['probability'] = cluster.qualifiers['probability'][0]
        if options.input_type == 'prot':
            js_cluster['unordered'] = True

        js_clusters.append(js_cluster)

    return js_clusters

def convert_cds_features(record, features, annotations, options):
    """Convert CDS SeqFeatures to JSON"""
    js_orfs = []
    for feature in features:
        js_orf = {}
        js_orf['start'] = int(feature.location.start) + 1
        js_orf['end'] = int(feature.location.end)
        js_orf['strand'] = feature.strand if feature.strand is not None else 1
        js_orf['locus_tag'] = utils.get_gene_id(feature)
        js_orf['type'] = get_biosynthetic_type(feature, annotations)
        js_orf['description'] = utils.ascii_string(get_description(record, feature, js_orf['type'], options))
        js_orfs.append(js_orf)
    return js_orfs

def get_description(record, feature, type_, options):
    "Get the description text of a feature"
    blastp_url = "http://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE=Proteins&" \
                 "PROGRAM=blastp&BLAST_PROGRAMS=blastp&QUERY=%s&" \
                 "LINK_LOC=protein&PAGE_TYPE=BlastSearch"
    genomic_context_url = "http://www.ncbi.nlm.nih.gov/projects/sviewer/?" \
                          "Db=gene&DbFrom=protein&Cmd=Link&noslider=1&"\
                          "id=%s&from=%s&to=%s"
    template = "%(product)s<br>\n"
    if options.smcogs:
        template += "smCOG: %(smcog)s<br>\n"
    if options.input_type == 'nucl':
        template += "Location: %(start)s - %(end)s<br>\n"
    if 'sec_met' in feature.qualifiers:
        template += "Signature pHMM hits:<br>\n%(model_details)s<br>\n"
    template += """
%(transport_blast_line)s
%(searchgtr_line)s
<a href="%(blastp_url)s" target="_new">NCBI BlastP on this gene</a><br>
<a href="%(genomic_context_url)s" target="_new">View genomic context</a><br>
%(smcog_tree_line)s<br>
<textarea cols="40" rows="%(len_seq)s" class="svgene-textarea">%(sequence)s</textarea>"""

    replacements = {
        'smcog': '-',
        'transport_blast_line': '',
        'smcog_tree_line': '',
        'searchgtr_line': '',
        'start': int(feature.location.start) + 1,
        'end': int(feature.location.end),
        'model_details': get_model_details(feature)
    }
    if not options.smcogs:
        del replacements['smcog']
    if options.input_type == 'prot':
        del replacements['start']
        del replacements['end']

    replacements['product'] = feature.qualifiers.get('product', ['-'])[0]
    if 'translation' in feature.qualifiers:
        sequence = feature.qualifiers['translation'][0]
    else:
        sequence = str(utils.get_aa_sequence(record, feature))
    replacements['blastp_url'] = blastp_url % sequence
    replacements['sequence'] = sequence
    replacements['len_seq'] = (len(sequence) / 40) + 1
    replacements['genomic_context_url'] = genomic_context_url % \
                    ( record.id,
                      max(feature.location.start - 9999, 0),
                      min(feature.location.end + 10000, len(record)) )

    if options.smcogs:
        for note in feature.qualifiers.get('note',[]):
            if note.startswith('smCOG:') and '(' in note:
                text, score = note[6:].split('(',1 )
                smcog, desc = text.split(':', 1)
                desc = desc.replace('_', ' ')
                replacements['smcog'] = '%s (%s)' % (smcog, desc)
            elif note.startswith('smCOG tree PNG image:'):
                entry = '<a href="%s" target="_new">View smCOG seed phylogenetic tree with this gene</a>'
                url = note.split(':')[-1]
                replacements['smcog_tree_line'] = entry % url

    if type_ == 'transport':
        url = "http://blast.jcvi.org/er-blast/index.cgi?project=transporter;" \
              "program=blastp;database=pub/transporter.pep;" \
              "sequence=sequence%%0A%s" % sequence
        transport_blast_line = '<a href="%s" target="_new">TransportDB BLAST on this gene<br>' % url
        replacements['transport_blast_line'] = transport_blast_line

    if options.searchgtr_links.has_key(record.id + "_" + utils.get_gene_id(feature)):
        url = options.searchgtr_links[record.id + "_" + utils.get_gene_id(feature)]
        searchgtr_line = '<a href="%s" target="_new">SEARCHGTr on this gene<br>' % url
        replacements['searchgtr_line'] = searchgtr_line

    return template % replacements


def get_biosynthetic_type(feature, annotations):
    "Get the biosythetic type of a CDS feature"
    ann = 'other'
    for note in feature.qualifiers.get('note', []):
        if not note.startswith('smCOG:'):
            continue

        smcog = note[7:].split(':')[0]
        ann = annotations.get(smcog, 'other')

    if not 'sec_met' in feature.qualifiers:
        return ann

    for qual in feature.qualifiers['sec_met']:
        if not qual.startswith('Kind:'):
            continue
        return qual[6:]
    return ann

def get_model_details(feature):
    result = ""
    for note in feature.qualifiers.get('sec_met', []):
        if not note.startswith('Domains detected'):
            continue
        note = note[18:]
        result += note.replace(';', '<br>')

    return result

def load_cog_annotations():
    "Load the smCOG type annotations from a file"
    type_keys = {
        'B': 'biosynthetic',
        'T': 'transport',
        'R': 'regulatory',
        'O': 'other'
    }
    annotations = {}
    for line in open(utils.get_full_path(__file__, 'cog_annotations.txt'), 'r'):
        line = line.strip()
        cog, description, type_ = line.split('\t', 3)
        annotations[cog] = type_keys.get(type_, 'other')

    return annotations
