#ifndef _MSCLASS_H_
#define _MSCLASS_H_

class MSClass
{
  private:
    float upperLimit;
    float lowerLimit;

  public:
    bool setRange(float minMass, float maxMass); 
  
    void writeToFile( const string &fname,
                      QueueClass< UnitClass > &list,
                      QueueClass< UnitClass > &output, 
                      string predictedSubstrates ) const;


    void setValues( const float inMin, const float inMax );

    float getUpperLimit() const;
    
    float getLowerLimit() const;

};

#include "MSClass.inl"
#endif
