#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <cmath>

using namespace std;

#include "spawnl.h"
#include "constants.h"
#include "fileInputOutput.h"
#include "UnitClass.h"
#include "QueueClass.h"
#include "ReactionClass.h"

float getFloatVal(string strConvert);
int getIntVal(string strConvert);
void readInSeq( string &inputSeq, bool readingSeq);
void translateSeq( string &inputSeq, string &outputSeq, int &frame);
void clearPositions( string &pos235, string &pos236, string &pos239,
        string &pos278, string &pos299, string &pos301, string &pos322,
        string &pos330, string &pos331, string &pos58, string &pos59,
        string &pos60, string &pos61, string &pos62, string &pos70,
        string &pos72, string &pos74, string &pos75, string &pos77,
        string &pos93, string &pos189, string &pos191, string &pos198,
        string &pos199, string &pos200 );
void clearPositions( string &pos235, string &pos236, string &pos239,
        string &pos278, string &pos299, string &pos301, string &pos322,
        string &pos330, string &pos331, string &pos58, string &pos59,
        string &pos60, string &pos61, string &pos62, string &pos70,
        string &pos72, string &pos74, string &pos75, string &pos77,
        string &pos93, string &pos189, string &pos191, string &pos198,
        string &pos199, string &pos200 )
{
  pos235 = "";
  pos236 = "";
  pos239 = "";
  pos278 = "";
  pos299 = "";
  pos301 = "";
  pos322 = "";
  pos330 = "";
  pos331 = "";
  pos58 = "";
  pos59 = "";
  pos60 = "";
  pos61 = "";
  pos62 = "";
  pos70 = "";
  pos72 = "";
  pos74 = "";
  pos75 = "";
  pos77 = "";
  pos93 = "";
  pos189 = "";
  pos191 = "";
  pos198 = "";
  pos199 = "";
  pos200 = "";
}

void predictDomains(int Aad, int Ala, int Asn, int Asp, int Cys,
                    int Dab, int Dhb, int Gln, int Glu, int Ile,
                    int Leu, int Orn, int Phe, int Phg, int Pip,
                    int Pro, int Ser, int Thr, int Tyr, int His,
                    int Lys, int Gly, int Val, string aminoAcidSeq,
                    string &predictedSubstrates,
                    QueueClass< UnitClass > &aaNameList, bool &validInput,
                    bool &quitLoop, const bool &allFramesRead,
                    const int &frame, string displayUnknown);

void outputSpecificitiesNRPS( int Aad, int Ala, int Asn, int Asp, int Cys,
			  int Dab, int Dhb, int Gln, int Glu, int Ile,
                          int Leu, int Orn, int Phe, int Phg, int Pip,
                          int Pro, int Ser, int Thr, int Tyr, int His,
                          int Lys, int Gly, int Val);

void outputSpecificitiesPKS( int mal, int mmal, int emal, int omal);

string maxSpecificityNRPS( int Aad, int Ala, int Asn, int Asp, int Cys,
		       int Dab, int Dhb, int Gln, int Glu, int Ile,
                       int Leu, int Orn, int Phe, int Phg, int Pip,
                       int Pro, int Ser, int Thr, int Tyr, int His,
                       int Lys, int Gly, int Val, string displayUnknown);

string maxSpecificityPKS( int mal, int mmal, int emal, int omal,
                          string displayUnknown);

float getFloatVal(string strConvert)
{
  float floatReturn;

  // NOTE: You should probably do some checks to ensure that
  // this string contains only numbers. If the string is not
  // a valid integer, zero will be returned.
  floatReturn = atof(strConvert.c_str());

  return(floatReturn);
}

int getIntVal(string strConvert)
{
  int intReturn;

  // NOTE: You should probably do some checks to ensure that
  // this string contains only numbers. If the string is not
  // a valid integer, zero will be returned.
  intReturn = atoi(strConvert.c_str());

  return(intReturn);
}


void outputSpecificitiesPKS( int mal, int mmal, int emal, int omal)
{
  cout << "PKS specificity scores: " << endl;
  cout << "mal: " << mal << " ";
  cout << "mmal: " << mmal << " ";
  cout << "emal: " << emal << " ";
  cout << "omal: " << omal << " ";
  cout << endl;
}
void outputSpecificitiesNRPS( int Aad, int Ala, int Asn, int Asp, int Cys,
			  int Dab, int Dhb, int Gln, int Glu, int Ile,
			  int Leu, int Orn, int Phe, int Phg, int Pip,
                          int Pro, int Ser, int Thr, int Tyr, int His,
                          int Lys, int Gly, int Val)
{
  cout << "NRPS specificity scores: " << endl;
  cout << "Aad: " << Aad << " ";
  cout << "Ala: " << Ala << " ";
  cout << "Asn: " << Asn << " ";
  cout << "Asp: " << Asp << " ";
  cout << "Cys: " << Cys << " ";
  cout << "Dab: " << Dab << " ";
  cout << "Dhb: " << Dhb << " ";
  cout << "Gln: " << Gln << " ";
  cout << "Glu: " << Glu << " ";
  cout << "Gly: " << Gly << " ";
  cout << "His: " << His << endl;
  cout << "Ile: " << Ile << " ";
  cout << "Leu: " << Leu << " ";
  cout << "Lys: " << Lys << " ";
  cout << "Orn: " << Orn << " ";
  cout << "Phe: " << Phe << " ";
  cout << "Phg: " << Phg << " ";
  cout << "Pip: " << Pip << " ";
  cout << "Pro: " << Pro << " ";
  cout << "Ser: " << Ser << " ";
  cout << "Thr: " << Thr << " ";
  cout << "Tyr: " << Tyr << endl;
  cout << "Val: " << Val << " ";
  cout << endl;
}

string maxSpecificityPKS( int mal, int mmal, int emal, int omal,
                          string displayUnknown)
{
  int max;
  string CA;
  max = mal;
  CA = "mal";

  if(mmal > mal)
  {
    max = mmal;
    CA = "mmal";
  }
  if(emal > max)
  {
    max = emal;
    CA = "emal";
  }
  if(omal > max)
  {
    max = omal;
    CA = "omal";
  }
  if(displayUnknown[0] == 'D' && displayUnknown[1] == 'U')
  {
    if( max < 5) // if score is less then five, it is uncertain
    {
      CA = "pk";
    }
  }
  return CA;
}

string maxSpecificityNRPS( int Aad, int Ala, int Asn, int Asp, int Cys,
		       int Dab, int Dhb, int Gln, int Glu, int Ile,
                       int Leu, int Orn, int Phe, int Phg, int Pip,
                       int Pro, int Ser, int Thr, int Tyr, int His,
                       int Lys, int Gly, int Val, string displayUnknown)
{
  int max;
  string AA;
  max = Aad;
  AA = "Aad";
  if(Ala >= Aad)
  {
    max = Ala;
    AA = "ala";
  }
  if(Asn >= max)
  {
    max = Asn;
    AA = "asn";
  }
  if(Asp >= max)
  {
    max = Asp;
    AA = "asp";
  }
  if(Cys >= max)
  {
    max = Cys;
    AA = "cys";
  }
  if(Dab >= max)
  {
    max = Dab;
    AA = "dab";
  }
  if(Dhb >= max)
  {
    max = Dhb;
    AA = "dhb";
  }
  if(Gln >= max)
  {
    max = Gln;
    AA = "gln";
  }
  if(Glu >= max)
  {
    max = Glu;
    AA = "glu";
  }
  if(Ile >= max)
  {
    max = Ile;
    AA = "ile";
  }
  if(Leu >= max)
  {
    max = Leu;
    AA = "leu";
  }
  if(Orn >= max)
  {
    max = Orn;
    AA = "orn";
  }
  if(Phe >= max)
  {
    max = Phe;
    AA = "phe";
  }
  if(Phg >= max)
  {
    max = Phg;
    AA = "phg";
  }
  if(Pip >= max)
  {
    max = Pip;
    AA = "pip";
  }
  if(Pro >= max)
  {
    max = Pro;
    AA = "pro";
  }
  if(Ser >= max)
  {
    max = Ser;
    AA = "ser";
  }
  if(Thr >= max)
  {
    max = Thr;
    AA = "thr";
  }
  if(Tyr >= max)
  {
    max = Tyr;
    AA = "tyr";
  }
  if(His >= max)
  {
    max = His;
    AA = "his";
  }
  if(Lys >= max)
  {
    max = Lys;
    AA = "lys";
  }
  if(Gly >= max)
  {
    max = Gly;
    AA = "gly";
  }
  if(Val >= max)
  {
    max = Val;
    AA = "val";
  }
  if(displayUnknown[0] == 'D' && displayUnknown[1] == 'U')
  {
    if( max < 3)
    {
      AA = "nrp";
    }
  }
  return AA;
}

void readInSeq( string &inputSeq, bool readingSeq)
{
  string partOfEntireSeq;

  while(!cin.fail() && readingSeq == 1)
  {
    cin >> partOfEntireSeq;
    inputSeq.insert(inputSeq.length(), partOfEntireSeq);

    if( inputSeq[inputSeq.length()-1] == 'x')
    {
      readingSeq = 0;
      inputSeq.erase(inputSeq.length(), 1);
    }
  }
  for(int i = 0; i < inputSeq.length(); i++)
  {
    if((static_cast< unsigned int >( inputSeq[i] ) >= 48 &&
	 static_cast< unsigned int >( inputSeq[i] ) <= 57 )
         || inputSeq[i] == '_' )
    {
      inputSeq.erase(i, 1);
      i--;
    }
  }
}

// translates DNA to amino acid through common code
void translateSeq( string &inputSeq, string &outputSeq, int &frame)
{
  int stopPrev = 0;
  int stopPres = 0;
  int stopLimit = 50;
  int start = 0;
  int choice;
  int stopCount = 0;
  int seqMin = 50;
  ifstream In;
  ofstream Out;
  string segment;
  outputSeq.clear();
  string inputFile = "clusters/" + inputSeq;
  //In.open(inputSeq.c_str());
  In.open(inputFile.c_str());

  /*while(In.fail())
  {
    cout << "Invalid file. Please select choice:" << endl;
    cout << "1. Input file" << endl;
    cout << "2. Exit" << endl;
    cin >> choice;

    if(choice == 1)
    {
      cin >> inputSeq;
      In.open(inputSeq.c_str());
    }
    else
    {
      return;
    }
  }*/

  In >> segment;
  while(segment.length() < seqMin  && !In.fail())
  {
    In >> segment;
  }

  inputSeq.append(segment);
  while(!In.fail())
  {
    In >> segment;
    inputSeq.append(segment);
  }

  //cout << "FASTA of DNA stored" << endl;
  In.close();
 //cout << "Frame: " << frame << endl;
 for(int i = frame; i < inputSeq.length()-3; i=i+3)
  {
    if(inputSeq[i] == 't' ||
       inputSeq[i] == 'T')
    {
      if(inputSeq[i+1] == 't' ||
         inputSeq[i+1] == 'T')
      {
        if(inputSeq[i+2] == 't' ||
           inputSeq[i+2] == 'T' ||
           inputSeq[i+2] == 'c' ||
           inputSeq[i+2] == 'C')
	{
          outputSeq.append("F");
	}
        else
	{
          outputSeq.append("L");
	}
      }
      else if(inputSeq[i+1] == 'c' ||
              inputSeq[i+1] == 'C')
      {
        outputSeq.append("S");
      }
      else if(inputSeq[i+1] == 'a' ||
              inputSeq[i+1] == 'A')
      {
        if(inputSeq[i+2] == 't' ||
           inputSeq[i+2] == 'T' ||
           inputSeq[i+2] == 'c' ||
           inputSeq[i+2] == 'C')
        {
          outputSeq.append("Y");
        }
        else
	{
          outputSeq.append("-");
          stopCount++; // stop codon
        }
      }
      else if(inputSeq[i+1] == 'g' ||
              inputSeq[i+1] == 'G')
      {
        if(inputSeq[i+2] == 't' ||
           inputSeq[i+2] == 'T' ||
           inputSeq[i+2] == 'c' ||
           inputSeq[i+2] == 'C')
	{
          outputSeq.append("C");
        }
        else if(inputSeq[i+2] == 'g' ||
                inputSeq[i+2] == 'G')
	{
          outputSeq.append("W");
        }
        else
	{
	  outputSeq.append("-");
          stopCount++; // stop codon

        }
      }
    } // sequence begins with 't' ends

    else if(inputSeq[i] == 'c' ||
    inputSeq[i] == 'C')
  {
    if(inputSeq[i+1] == 't' ||
       inputSeq[i+1] == 'T')
    {
      outputSeq.append("L");
    }
    else if(inputSeq[i+1] == 'c' ||
            inputSeq[i+1] == 'C')
    {
      outputSeq.append("P");
    }
    else if(inputSeq[i+1] == 'a' ||
            inputSeq[i+1] == 'A')
    {
      if(inputSeq[i+2] == 't' ||
         inputSeq[i+2] == 'T' ||         inputSeq[i+2] == 'c' ||
         inputSeq[i+2] == 'C')
      {
        outputSeq.append("H");
      }
      else
      {
        outputSeq.append("Q");
      }
    }
    else if(inputSeq[i+1] == 'g' ||
            inputSeq[i+1] == 'G')
    {
      outputSeq.append("R");
    }
  } // sequence begins with 'c' ends

    else if(inputSeq[i] == 'a' ||
       inputSeq[i] == 'A')
    {
      if(inputSeq[i+1] == 't' ||
         inputSeq[i+1] == 'T')
      {
        if(inputSeq[i+2] != 'g' &&
           inputSeq[i+2] != 'G')
	{
          outputSeq.append("I");
	}
        else
	{
            outputSeq.append("M");
        }
      }
      else if(inputSeq[i+1] == 'c' ||
              inputSeq[i+1] == 'C')
      {
        outputSeq.append("T");
      }
      else if(inputSeq[i+1] == 'a' ||
              inputSeq[i+1] == 'A')
      {
        if(inputSeq[i+2] == 't' ||
           inputSeq[i+2] == 'T' ||
           inputSeq[i+2] == 'c' ||
           inputSeq[i+2] == 'C')
        {
          outputSeq.append("N");
        }
        else
	{
          outputSeq.append("K");
        }
      }
      else if(inputSeq[i+1] == 'g' ||
              inputSeq[i+1] == 'G')
      {
        if(inputSeq[i+2] == 't' ||
           inputSeq[i+2] == 'T' ||
           inputSeq[i+2] == 'c' ||
           inputSeq[i+2] == 'C')
	{
          outputSeq.append("S");
        }
        else
	{
          outputSeq.append("R");
        }
      }
    } // sequence begins with 'a' ends

    else if(inputSeq[i] == 'g' ||
       inputSeq[i] == 'G')
    {
      if(inputSeq[i+1] == 't' ||
         inputSeq[i+1] == 'T')
      {
        outputSeq.append("V");
      }
      else if(inputSeq[i+1] == 'c' ||
              inputSeq[i+1] == 'C')
      {
        outputSeq.append("A");
      }
      else if(inputSeq[i+1] == 'a' ||
              inputSeq[i+1] == 'A')
      {
        if(inputSeq[i+2] == 't' ||
           inputSeq[i+2] == 'T' ||
           inputSeq[i+2] == 'c' ||
           inputSeq[i+2] == 'C')
        {
          outputSeq.append("D");
        }
        else
        {
          outputSeq.append("E");
        }
      }
      else if(inputSeq[i+1] == 'g' ||
              inputSeq[i+1] == 'G')
      {
        outputSeq.append("G");
      }
    } // codon begins with 'g' ends
    else
    {
      outputSeq.append("-");
      stopCount++;
    }
  } // for loop ends

Out.open("aminoAcidSeq.txt");
Out.clear();
Out << "Protein from one DNA frame" << "\n";
Out << outputSeq;
Out.close();

} // function ends

// get user input of filename
string getStringFromUser( const string &label )
{
  string filename;
  bool validInput = false;
  cout << "Enter filename for " << label << ": ";
  while ( !validInput )
  {
    cin >> filename;
    if ( cin.fail() )
    {
      cin.clear();
      cin.ignore(256, '\n');
      cout << "Invalid data entered" << endl;
      cout << "Enter filename for " << label << ": ";
      validInput = false;
    }
    else
    {
      validInput = true;
    }
  }
  return( filename );
}
void predictDomains(int Aad, int Ala, int Asn, int Asp, int Cys,
                   int Dab, int Dhb, int Gln, int Glu, int Ile,
                   int Leu, int Orn, int Phe, int Phg, int Pip,
                   int Pro, int Ser, int Thr, int Tyr, int His,
                   int Lys, int Gly, int Val, string aminoAcidSeq,
                   string &predictedSubstrates,
                   QueueClass< UnitClass > &aaNameList, bool &validInput,
                   bool &quitLoop, const bool &allFramesRead,
                   const int &frame, string displayUnknown)
{
  QueueClass <UnitClass> sortedListOfDomains;
  QueueClass <UnitClass> sortedListOfMods;
  UnitClass *domain;
  ifstream In;
  ifstream InMods;
  ifstream tempIn;
  ofstream Out;
  ofstream tempOut;
  string segment;
  float idenScore;
  float score;
  string idenScoreString;
  string scoreString;
  float modsScore;
  float scoreMax = 0.1; // minimum for domain synthetase matching
  float modsScoreMax = 0.1; // minimum for mods alignment
  int idenScoreMin = 100; // minimal length of homology
  int length;
  string SMILE;
  string formula;
  string mod;
  string substrate;
  float siteToAnalyze;
  int siteCount;
  bool siteFound;
  int whileCount = 0;
  int queryOrSbjctLength = 6;
  string aaSeq;
  char *arg[21];
  arg[0] = "./blastall"; // program to execute
  arg[1] = "blastp"; // protein blast
  arg[2] = "synthetases.fasta"; // control A-domain
  arg[3] = "mods.fasta"; // output file
  arg[4] = "100.0";
  arg[6] = "aaSeq.txt";
  arg[7] = "blast2.out";
  int AdomainStart = 5;
  int AdomainEnd = 5;
  // A-domains to analyze

  bool substrateKnown;
  string query;
  string sbjct;
  string predictedSubstrate;
  string proposedSubstrate;
  bool foundSubstrate;
  bool stringsCompare;
  int hits = 0;
  int misses = 0;
  int maxChars = 10000;
  // NRPS key positions
  int p235 = 11;
  int p236 = 12;
  int p239 = 15;
  int p278 = 10;
  int p299 = 1;
  int p301 = 3;
  int p322 = 1;
  int p330 = 9;
  int p331 = 10;
  string pos235;
  string pos236;
  string pos239;
  string pos278;
  string pos299;
  string pos301;
  string pos322;
  string pos330;
  string pos331;
   int Gly1 = 0;
   int Gly2 = 0;
   int Gly3 = 0;
   int Leu1 = 0;
   int Leu2 = 0;
   int Leu3 = 0;
   int Leu4 = 0;
   int Tyr1 = 0;
   int Tyr2 = 0;
   int Tyr3 = 0;
   int Val1 = 0;
   int Val2 = 0;
   int Val3 = 0;
   int mal = 0;
   int mmal = 0;
   int emal = 0;
   int omal = 0;
   char FDxAA;
   char FD4xAA;
   char IT3xAA;
   char x1xG299;
   char x1xG301;
   char x7xxY322;
   char x7xxY330;
   char x7xxY331;
   string scoredSubstrate;
  // PKS key positions
  int p58 = 0;
  int p59 = 1;
  int p60 = 2;
  int p61 = 3;
  int p62 = 4;
  int p70 = 0;
  int p72 = 2;
  int p74 = 4;
  int p75 = 5;
  int p77 = 7;
  int p93 = 3;
  int p189 = 1;
  int p191 = 3;
  int p198 = 0;
  int p199 = 1;
  int p200 = 2;
  string pos58;
  string pos59;
  string pos60;
  string pos61;
  string pos62;
  string pos70;
  string pos72;
  string pos74;
  string pos75;
  string pos77;
  string pos93;
  string pos189;
  string pos191;
  string pos198;
  string pos199;
  string pos200;
  string sigSeq;
  int position;
  bool NRPS = 0;
  bool PKS = 0;
  string residue;
  string fname;
  bool success;
  string smileName;
  string inString;
  UnitClass *aaName;
  float currentDomainPos;
  float nextDomainPos;
  float thirdDomainPos;
  int modRange = 5000;
  bool KR = 0;
  bool DH = 0;
  bool ER = 0;
  bool HY = 0;
  int condCount = 0;
  int lastPKSkey = 3;
  int minNrpSigSeqLength = 0;
  int minPkSigSeqLength = 0;
  bool sufficientSigSeqLength = 1;
  bool moddedAlready = 0;

  //cout << "ALL FRAMES READ: " << allFramesRead << endl;

  //get every NRPS/PKS domain until no further domains are detected;

  // does blast alignment of cluster

  spawnl( 1, "./blastall", "blastall",
          "-g", "T", "-p", arg[1], "-d", arg[2], "-i", aminoAcidSeq.c_str(),
          "-F", "F",  "-C", "0", "-o", "blast.out", NULL);
  spawnl( 1, "./blastall", "blastall",
          "-g", "T", "-p", arg[1], "-d", arg[3], "-i", aminoAcidSeq.c_str(),
          "-F", "F",  "-C", "0", "-o", "modsBlast.out", NULL);

  if(frame == 0)
  {
    system("cp blast.out frame0.out");
    system("cp modsBlast.out modsFrame0.out");
  }
  if(frame == 1)
  {
    system("cp blast.out frame1.out");
    system("cp modsBlast.out modsFrame1.out");
  }
  if(frame == 2)
  {
    system("cp blast.out frame2.out");
    system("cp modsBlast.out modsFrame2.out");
    system("cat frame0.out frame1.out frame2.out > blast.out");
    system("cat modsFrame0.out modsFrame1.out modsFrame2.out > modsBlast.out");
    system("rm frame0.out frame1.out frame2.out modsFrame1.out modsFrame2.out");
  }

if(allFramesRead)
{
  score = 0;
  // open NRPS/PKS alignment file
  In.open("blast.out");
  In >> segment;
  while( !In.fail())
  {
    if(segment[0] == '*')
    {
      In >> segment;
      substrate = segment;
    }
    if(segment[0] == 'E' && segment[1] == 'x'
       && segment[2] == 'p')
    {
      In >> segment; // "="
      In >> segment; // expectation-value

      for(int i = 0; i < segment.length(); i++)
      {
        if(segment[i] == 'e')
        {
          i = segment.length();
        }
        else
        {
          scoreString.append(1, segment[i]);
        }
      }
      score = getFloatVal(scoreString);
      scoreString.clear();
      for(int i = 0; i < segment.length(); i++)
      {
        if(segment[i] == 'e')
        {
          for(int j = i+1; j < segment.length(); j++)
          {
            scoreString.append(1, segment[j]);
            i = j;
          }
        }
      }
      score = score * pow(10.0,(static_cast<double>(getFloatVal(scoreString))));
      scoreString.clear();
     while(segment[0] != 'I')
     {
        In >> segment;
     }
     if(segment[0] == 'I' && segment[1] == 'd'
        && segment[2] == 'e')
     {
       In >> segment; // "="
       In >> segment; // identities

       // collect denominator of identities value
       for(int i = 0; i < segment.length(); i++)
       {
         if(segment[i] == '/')
         {
           for(int j = i+1; j < segment.length(); j++)
           {
             idenScoreString.append(1, segment[j]);
             i = j;
           }
         }
       }
     }
     idenScore = getIntVal(idenScoreString);
     idenScoreString.clear();

      if(score < scoreMax && idenScore > idenScoreMin)
      {
        while( segment[0] != 'Q')
        {
          In >> segment;
        }
        In >> segment; // start site of domain on protein
        length = getIntVal(segment);
        cout << "e-value: " << score << " idenScore: " << idenScore;
        cout << " start site: " << length << endl;
        domain = new UnitClass;
        domain->setMass(length);
        domain->setFormula(substrate);
        sortedListOfDomains.insertValue(*domain);
        delete domain;
      }
      score = 1; // reset scores
      idenScore = 0;
    }
    In >> segment;
  }

  In.close();
  sortedListOfDomains.printForward();

  // detect modifications in cluster and their locations
  InMods.open("modsBlast.out");
  InMods >> segment;
  //cout << "InMods: " << InMods << " segment: " << segment << endl;
  while( !InMods.fail())
  {
    if(segment[0] == '*')
    {
      InMods >> segment;
      mod = segment;
    }
    if(segment[0] == 'E' && segment[1] == 'x'
       && segment[2] == 'p')
    {
      InMods >> segment; // "="
      InMods >> segment; // expectation

      for(int i = 0; i < segment.length(); i++)
      {
        if(segment[i] == 'e')
        {
          i = segment.length();
        }
        else
        {
          scoreString.append(1, segment[i]);
        }
      }
      score = getFloatVal(scoreString);
      scoreString.clear();
      for(int i = 0; i < segment.length(); i++)
      {
        if(segment[i] == 'e')
        {
          for(int j = i+1; j < segment.length(); j++)
          {
            scoreString.append(1, segment[j]);
            i = j;
          }
        }
      }

      score = score * pow(10.0,(static_cast<double>(getFloatVal(scoreString))));
      scoreString.clear();
      while(segment[0] != 'I')
      {
        InMods >> segment;
      }
      if(segment[0] == 'I' && segment[1] == 'd'
        && segment[2] == 'e')
      {
        InMods >> segment; // "="
        InMods >> segment; // identities

        // collect denominator of identities value
        for(int i = 0; i < segment.length(); i++)
        {
          if(segment[i] == '/')
          {
            for(int j = i+1; j < segment.length(); j++)
            {
              idenScoreString.append(1, segment[j]);
              i = j;
            }
          }
        }
      }
      idenScore = getIntVal(idenScoreString);
      idenScoreString.clear();
      if(score < modsScoreMax && (idenScore > idenScoreMin
          || mod[0] == 'T' && mod[1] == 'E'))
      {
        while( segment[0] != 'Q')
        {
          InMods >> segment;
        }
        InMods >> segment; // start site of domain on protein
        cout << "e-value: " << score << " idenScore: " << idenScore;
        cout << " mod start site: " << segment << endl;
        length = getIntVal(segment);
        domain = new UnitClass;
        domain->setMass(length);
        domain->setFormula(mod);
        sortedListOfMods.insertValue(*domain);
        delete domain;
      }
      score = 1; // reset score
      idenScore = 0;
    }
    InMods >> segment;
  }
  InMods.clear();
  InMods.close();
  sortedListOfMods.printForward();

  siteCount = 0;
  // go through each domain and collect and align each
  while(siteCount < sortedListOfDomains.getNumElems() && whileCount < 1000000)
  {
    In.close();
    In.open("blast.out");
    In.clear();
    In >> segment;
    siteFound = 0;
    while( !In.fail() && siteFound == 0 && whileCount < 1000000)
    {
      siteToAnalyze = sortedListOfDomains.getElemAtIndex(siteCount).getMass();

      if(segment[0] == 'Q' && segment[1] == 'u'
         && segment[queryOrSbjctLength - 1] == ':')
      {
        In >> segment;
        if(getIntVal(segment) == siteToAnalyze)
	{
          // analyze this segment only and add to aaNameList
          // store any modifications between this domain and the next
          cout << "segment " << segment << " found " << endl;
          position = getIntVal(segment);
          siteFound = 1;
          siteCount++;

          In >> segment;
          aaSeq.append(segment);
          In >> segment;
          while((segment[0] != 'S' || segment[1] != 'c') && (segment[0] != 'D' || segment[1] != 'a') && !In.fail())
	  {
            //cout << "segment2: " << segment << endl;
            if(segment[0] == 'Q' && segment[1] == 'u'
               && segment[queryOrSbjctLength - 1] == ':')
	    {
              In >> segment;
              In >> segment;
              //cout << "segment: " << segment << endl;
              for(int i = 0; i < segment.length(); i++)
	      {
                if((static_cast< unsigned int >(segment[i]) < 65
		    || static_cast< unsigned int >(segment[i]) > 90)
		    && (static_cast< unsigned int >(segment[i]) < 97
			|| static_cast< unsigned int >(segment[i]) > 122))
		{
                  segment.erase(i,1);
                  i--;
                }
              }
	      aaSeq.append(segment);
            }
            In >> segment;
          }

	  // recognize amino acid sequence here
          cout << "aaSeq: " << aaSeq << endl;
          Out.open("aaSeq.txt");
          Out.clear();
          Out << "> " << "aaSeq" << '\n' << aaSeq;
          Out.close();
          aaSeq.clear();

          // does blast alignment of individual domain to recognize
          // conserved motifs and substrate specificity
          spawnl( 1, "./blastall", "blastall",
          "-g", "T", "-p", arg[1], "-d", arg[2], "-i", "aaSeq.txt",
          "-F", "F",  "-C", "0", "-o", "blast2.out", NULL);
          substrateKnown = 0;
          In.close();
          In.open("blast2.out");
          In.clear();
          segment.clear();
          In >> segment;

          while(!In.fail())
          {
            ///cout << "111" << endl;

            //cout << !In.fail() << " ";
            //cout << segment << endl;

            if(segment == "NRPS" && PKS == 0 && NRPS == 0)
            {
              NRPS = 1;
              cout << "NRPS detected" << endl;
            }
            if(segment == "PKS" && NRPS == 0 && PKS == 0)
            {
              PKS = 1;
              cout << "PKS detected" << endl;
            }
            if(segment.length() == queryOrSbjctLength)
            {
              if(segment[0] == 'Q' && segment[1] == 'u'
                 && segment[queryOrSbjctLength - 1] == ':')
              {
                In >> segment;
                In >> segment;
                query.append(segment);
                //cout << "Query: " << segment << endl;
              }
              if(segment[0] == 'S' && segment[1] == 'b')
              {
                In >> segment;
                In >> segment;
                sbjct.append(segment);
                //cout << "Sbjct: " << segment << endl;
              }
            }
            In >> segment;
          }
          In.clear();
          In.close();
          segment.clear();

          // search for first FDX motif containing residues 235,236,239

          if(NRPS)
          {
            if(sbjct.find("SRVLQFASFSFD") > 0
            && sbjct.find("SRVLQFASFSFD") < maxChars   )
            {
              pos235 = query[sbjct.find("SRVLQFASFSFD")+p235];
              pos236 = query[sbjct.find("SRVLQFASFSFD")+p236];
              pos239 = query[sbjct.find("SRVLQFASFSFD")+p239];
              sigSeq.append(pos235);
              sigSeq.append(pos236);
              sigSeq.append(pos239);
            }
            // search for IT motif containing the residue 278
            if(sbjct.find("RLKNDGITHVTLPP") > 0
               && sbjct.find("RLKNDGITHVTLPP") < maxChars   )
            {
              pos278 = query[sbjct.find("RLKNDGITHVTLPP")+p278];
              sigSeq.append(pos278);
            }

            // search for GE motif containing the residues 299,301
            if(sbjct.find("IVVQGEACP") > 0
              && sbjct.find("IVVAGEACP") < maxChars   )
            {
              pos299 = query[sbjct.find("IVVAGEACP")+p299];
              pos301 = query[sbjct.find("IVVAGEACP")+p301];
              sigSeq.append(pos299);
              sigSeq.append(pos301);
            }

            // search for YGPTE motif containing the residues 322,330,331
            if(sbjct.find("NAYGPTE") > 0
               && sbjct.find("NAYGPTE") < maxChars   )
            {
              pos322 = query[sbjct.find("NAYGPTE")+p322];
              pos330 = query[sbjct.find("NAYGPTE")+p330];
              pos331 = query[sbjct.find("NAYGPTE")+p331];
              sigSeq.append(pos322);
              sigSeq.append(pos330);
              sigSeq.append(pos331);
           }
         }
         if(PKS)
         {
           if(sbjct.find("DVPLV") > 0
              && sbjct.find("DVPLV") < maxChars   )
           {
             pos58 = query[sbjct.find("DVPLV")+p58];
             pos59 = query[sbjct.find("DVPLV")+p59];
             pos60 = query[sbjct.find("DVPLV")+p60];
             pos61 = query[sbjct.find("DVPLV")+p61];
             pos62 = query[sbjct.find("DVPLV")+p62];
             sigSeq.append(pos58);
             sigSeq.append(pos59);
             sigSeq.append(pos60);
             sigSeq.append(pos61);
             sigSeq.append(pos62);
          }
          if(sbjct.find("QVAL") > 0
          && sbjct.find("QVAL") < maxChars   )
          {
            pos70 = query[sbjct.find("QVAL")+p70];
            pos72 = query[sbjct.find("QVAL")+p72];
            pos74 = query[sbjct.find("QVAL")+p74];
            pos75 = query[sbjct.find("QVAL")+p75];
            pos77 = query[sbjct.find("QVAL")+p77];
            sigSeq.append(pos70);
            sigSeq.append(pos72);
            sigSeq.append(pos74);
            sigSeq.append(pos75);
            sigSeq.append(pos77);
         }
         if(sbjct.find("GQSMGE") > 0
         && sbjct.find("GQSMGE") < maxChars   )
         {
           pos93 = query[sbjct.find("GQSMGE")+p93];
           sigSeq.append(pos93);
         }
         if(sbjct.find("ITVRRI") > 0
         && sbjct.find("ITVRRI") < maxChars   )
         {
           pos189 = query[sbjct.find("ITVRRI")+p189];
           sigSeq.append(pos189);
           pos191 = query[sbjct.find("ITVRRI")+p191];
           sigSeq.append(pos191);
         }
         if(sbjct.find("YASH") > 0
          && sbjct.find("YASH") < maxChars   )
         {
           pos198 = query[sbjct.find("YASH")+p198];
           pos199 = query[sbjct.find("YASH")+p199];
           pos200 = query[sbjct.find("YASH")+p200];
           sigSeq.append(pos198);
           sigSeq.append(pos199);
           sigSeq.append(pos200);
         }
       }
       FDxAA = pos236[0];
       FD4xAA = pos239[0];
       IT3xAA = pos278[0];
       x1xG299 = pos299[0];
       x1xG301 = pos301[0];
       x7xxY322 = pos322[0];
       x7xxY330 = pos330[0];
       x7xxY331 = pos331[0];

       cout << "signature sequence: " << sigSeq << endl;
       Out.open("sigSeq.txt");
       Out.clear();
       Out << "> " << "SigSeq" << '\n'
           << sigSeq;
       Out.close();

       // align with NRPS/PKS signatures
       if(PKS)
       {
           spawnl(1, "./blastall", "blastall", "-p", "blastp", "-d",
              "PKSsignatures.fasta", "-i", "sigSeq.txt",
	      "-e", arg[4], "-F", "F", "-C", "0", "-e", "10.0",
	      "-o", "blast3.out", NULL);

         /* cout << pos58[0] << pos59[0] << pos60[0] << pos61[0]
              << pos61[0] << pos62[0] << pos70[0] << pos72[0]
              << pos74[0] << pos75[0] << pos77[0] << pos93[0]
              << pos189[0] << pos191[0] << pos189[0] << pos191[0]
              << pos198[0] << pos199[0] << pos200[0] << endl; */

         if(pos58[0] == 'E' || pos58[0] == 'D' || pos58[0] == 'Q'
            || pos58[0] == 'S' || pos58[0] == 'K')
         {
           mal++;
         }
         if(pos58[0] == 'R' || pos58[0] == 'G')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos59[0] == 'T' || pos59[0] == 'S')
         {
           mal++;
         }
         if(pos59[0] == 'V' || pos59[0] == 'A')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos60[0] == 'G' || pos60[0] == 'P' || pos60[0] == 'A'
            || pos60[0] == 'L' || pos60[0] == 'R')
         {
           mal++;
         }
         if(pos60[0] == 'D' || pos60[0] == 'E')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos61[0] == 'Y' || pos61[0] == 'F')
         {
           mal++;
         }
         if(pos61[0] == 'V')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos62[0] == 'A' || pos62[0] == 'T')
         {
           mal++;
         }
         if(pos62[0] == 'V')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos70[0] == 'Q')
         {
           mal++;
         }
         if(pos70[0] == 'M')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos72[0] == 'A')
         {
           mal++;
         }
         if(pos72[0] == 'S')
         {
           mmal++;
         }
         if(pos74[0] == 'F')
         {
           mal++;
         }
         if(pos74[0] == 'A')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos75[0] == 'G')
         {
           mal++;
         }
         if(pos75[0] == 'A')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos77[0] == 'L')
         {
           mal++;
         }
         if(pos77[0] == 'W')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos93[0] == 'L' || pos93[0] == 'V' || pos93[0] == 'I')
         {
           mal++;
         }
         if(pos93[0] == 'Q')
         {
           mmal++;
           emal++;
           omal++;
         }
         if(pos189[0] == 'M' || pos189[0] == 'L' || pos189[0] == 'Y')
         {
           omal++;
         }
         else
         {
           mal++;
           mmal++;
           emal++;
         }
         if(pos191[0] == 'W')
         {
           omal++;
         }
         else
         {
           mal++;
           mmal++;
           emal++;
         }
         if(pos198[0] == 'H')
         {
           mal = mal + lastPKSkey;
         }
         if(pos198[0] == 'Y')
         {
           mmal = mmal + lastPKSkey;
         }
         if(pos198[0] == 'T' || pos198[0] == 'C')
         {
           emal = emal + lastPKSkey;
         }
         if(pos199[0] == 'P')
         {
           emal++;
         }
         if(pos200[0] == 'F')
         {
           mal = mal + lastPKSkey;
         }
         if(pos200[0] == 'S')
         {
           mmal = mmal + lastPKSkey;
         }
         if(pos200[0] == 'G')
         {
           emal = emal + lastPKSkey;
         }
         if(pos200[0] == 'T')
         {
           emal++;
         }

         outputSpecificitiesPKS( mal, mmal, emal, omal);
         scoredSubstrate = maxSpecificityPKS(mal, mmal, emal, omal, displayUnknown);

         mal = 0;
         mmal = 0;
         emal = 0;
         omal = 0;
         clearPositions( pos235, pos236, pos239,
         pos278, pos299, pos301, pos322, pos330,
         pos331, pos58, pos59, pos60, pos61, pos62,
         pos70, pos72, pos74, pos75, pos77, pos93,
         pos189, pos191, pos198, pos199, pos200 );

       }
       if(NRPS)
       {
         spawnl(1, "./blastall", "blastall", "-p", "blastp", "-d",
              "NRPSsignatures.fasta", "-i", "sigSeq.txt",
	      "-e", arg[4], "-F", "F", "-C", "0", "-e", "10.0",
	      "-o", "blast3.out", NULL);

        if(FDxAA == 'P' || FDxAA == 'p')
	{
          Aad++;
        }
        if(FDxAA == 'L' || FDxAA == 'l')
	{
          Ala++;
          Asn++;
          Asp++;
          Cys++;
          Dab++;
          Dhb++;
          Gly3++;
        }
        if(FDxAA == 'H' || FDxAA == 'h')
	{
          Cys++;
        }
        if(FDxAA == 'A' || FDxAA == 'a')
	{
          Gln++;
          Glu++;
          Ile++;
          Leu1++;
          Leu2++;
          Leu4++;
          Phe++;
          Tyr2++;
          Tyr3++;
          Val1++;
          Val3++;
          Lys++;
        }
        if(FDxAA == 'G' || FDxAA == 'g')
	{
          Ile++;
          Leu3++;
          Tyr1++;
        }
        if(FDxAA == 'M' || FDxAA == 'm')
	{
          Orn++;
        }
        if(FDxAA == 'V' || FDxAA == 'v')
	{
          Orn++;
          Pro++;
          Ser++;
	}
        if(FDxAA == 'I' || FDxAA == 'i')
	{
          Phg++;
          Gly1++;
          Gly2++;
        }
        if(FDxAA == 'F' || FDxAA == 'f')
	{
          Phe++;
          Thr++;
          Val2++;
        }
        if(FDxAA == 'S' || FDxAA == 's')
	{
          His++;
        }

      if(FD4xAA == 'R' || FD4xAA == 'r')
      {
        Aad++;
      }
      if(FD4xAA == 'L' || FD4xAA == 'l')
      {
        Ala++;
        Tyr2++;
        Gly1++;
      }
      if(FD4xAA == 'T' || FD4xAA == 't')
      {
        Asn++;
        Asp++;
        Tyr1++;
      }
      if(FD4xAA == 'E' || FD4xAA == 'e')
      {
        Cys++;
        Dab++;
        Orn++;
        Val2++;
        His++;
        Lys++;
      }
      if(FD4xAA == 'Y' || FD4xAA == 'y')
      {
        Cys++;
      }
      if(FD4xAA == 'P' || FD4xAA == 'p')
      {
        Dhb++;
      }
      if(FD4xAA == 'Q' || FD4xAA == 'q')
      {
        Gln++;
        Pip++;
        Pro++;
        Gly2++;
      }
      if(FD4xAA == 'W' || FD4xAA == 'w')
      {
        Glu++;
        Leu1++;
        Leu2++;
        Phe++;
        Ser++;
	Thr++;
        Val3++;
        Gln++;
      }
      if(FD4xAA == 'K' || FD4xAA == 'k')
      {
        Glu++;
      }
      if(FD4xAA == 'F' || FD4xAA == 'f')
      {
        Ile++;
        Leu4++;
        Gly3++;
        Val1++;
      }
      if(FD4xAA == 'A' || FD4xAA == 'a')
      {
        Leu3++;
      }
      if(FD4xAA == 'G' || FD4xAA == 'g')
      {
        Orn++;
      }
      if(FD4xAA == 'S' || FD4xAA == 's')
      {
        Tyr3++;
      }
           // score IT3xAA
        if(IT3xAA == 'N' || IT3xAA == 'n')
	{
          Aad++;
          Cys++;
          Orn++;
          Thr++;
          Gly3++;
        }
        if(IT3xAA == 'F' || IT3xAA == 'f')
	{
          Ala++;
          Ile++;
          Leu++;
        }
        if(IT3xAA == 'K' || IT3xAA == 'k')
	{
          Asn++;
          Asp++;
        }
        if(IT3xAA == 'S' || IT3xAA == 's')
	{
          Cys++;
          Val++;
          Lys++;
	}
        if(IT3xAA == 'H' || IT3xAA == 'h')
	{
          Dab++;
          Glu++;
          Ser++;
	}
        if(IT3xAA == 'A' || IT3xAA == 'a')
	{
          Dhb++;
          His++;
        }
        if(IT3xAA == 'D' || IT3xAA == 'd')
	{
          Gln++;
          Glu++;
        }
        if(IT3xAA == 'L' || IT3xAA == 'l')
	{
          Leu++;
          Phg++;
          Pip++;
          Pro++;
        }
        if(IT3xAA == 'Y' || IT3xAA == 'y')
	{
          Leu++;
        }
        if(IT3xAA == 'M' || IT3xAA == 'm')
	{
          Leu++;
          Val++;
          Gly2++;
        }
        if(IT3xAA == 'E' || IT3xAA == 'e')
	{
          Orn++;
        }
        if(IT3xAA == 'T' || IT3xAA == 't')
	{
          Phe++;
          Tyr++;
        }
        if(IT3xAA == 'I' || IT3xAA == 'i')
	{
          Tyr++;
        }
        if(IT3xAA == 'V' || IT3xAA == 'v')
	{
          Tyr++;
        }
        if(IT3xAA == 'W' || IT3xAA == 'w')
	{
          Val++;
        }
        if(IT3xAA == 'Q' || IT3xAA == 'q')
	{
          Gly1++;
          Gln++;
        }

        if(x1xG299 == 'I' || x1xG299 == 'i')
	{
          Aad++;
          Orn++;
          Phe++;
          Pro++;
          Thr++;
          Val1++;
        }
        if(x1xG299 == 'G' || x1xG299 == 'g')
	{
          Ala++;
        }
        if(x1xG299 == 'L' || x1xG299 == 'l')
	{
          Asn++;
	  Cys++;
          Glu++;
          Ile++;
          Leu1++;
          Leu4++;
          Orn++;
          Phg++;
          Pip++;
          Ser++;
          Gly1++;
        }
        if(x1xG299 == 'V' || x1xG299 == 'v')
	{
          Asp++;
          Phe++; // added from Challis
          Tyr3++;
        }
        if(x1xG299 == 'D' || x1xG299 == 'd')
	{
          Cys++;
        }
        if(x1xG299 == 'N' || x1xG299 == 'n')
	{
          Dab++;
          Gly3++;
        }
        if(x1xG299 == 'Q' || x1xG299 == 'q')
	{
          Dhb++;
        }
        if(x1xG299 == 'F' || x1xG299 == 'f')
	{
          Glu++;
          Val3++;
          Gly2++;
          Gln++;
        }
        if(x1xG299 == 'Y' || x1xG299 == 'y')
	{
          Ile++;
          Leu2++;
        }
        if(x1xG299 == 'T' || x1xG299 == 't')
	{
          Leu3++;
          Tyr1++;
          Tyr2++;
          Val2++;
          His++;
        }

        // detect 301 signature
        if(x1xG301 == 'V' || x1xG301 == 'v')
	{
          Aad++;
          Cys++;
          Gly2++;
        }
        if(x1xG301 == 'I' || x1xG301 == 'i')
	{
          Ala++;
          Cys++;
          Lys++;
        }
        if(x1xG301 == 'G' || x1xG301 == 'g')
	{
          Asn++;
          Asp++;
          Dhb++;
          Gln++;
          Glu++;
          Ile++;
          Leu1++;
          Leu2++;
          Leu3++;
          Leu4++;
          Orn++;
          Phg++;
          Phe++; // extra
          Pip++;
          Thr++;
          Tyr2++;
          Val1++;
          Lys++;
          Gly1++;
        }
        if(x1xG301 == 'S' || x1xG301 == 's')
	{
          Cys++;
          Ser++;
        }
        if(x1xG301 == 'T' || x1xG301 == 't')
	{
          Dab++;
        }
        if(x1xG301 == 'A' || x1xG301 == 'a')
	{
          Phe++;
          Pro++;
          Tyr1++;
          Tyr3++;
          Val2++;
          Val3++;
          His++;
          Gly3++;
        }

        // analyze 322 residue
        if(x7xxY322 == 'E' || x7xxY322 == 'e')
	{
          Aad++;
          Asn++;
          Leu3++;
          Tyr1++;
          His++;
        }
        if(x7xxY322 == 'A' || x7xxY322 == 'a')
	{
          Ala++;
          Leu2++;
          Phe++;
          Tyr2++;
          Tyr3++;
          Val2++;
          Val3++;
          Gly2++;
        }
        if(x7xxY322 == 'H' || x7xxY322 == 'h')
	{
          Asp++;
          Pro++;
        }
        if(x7xxY322 == 'G' || x7xxY322 == 'g')
	{
          Cys++;
          Glu++;
          Val1++;
          Phe++; // extra
        }
        if(x7xxY322 == 'L' || x7xxY322 == 'l')
	{
          Cys++;
          Orn++;
          Phg++;
          Ser++;
          Gly1++;
          Gly3++;
          Gln++;
        }
        if(x7xxY322 == 'T' || x7xxY322 == 't')
	{
          Dab++;
        }
        if(x7xxY322 == 'V' || x7xxY322 == 'v')
	{
          Dhb++;
          Gln++;
          Glu++;
          Ile++;
          Pip++;
        }
        if(x7xxY322 == 'I' || x7xxY322 == 'i')
	{
          Ile++;
          Gln++;
        }
        if(x7xxY322 == 'N' || x7xxY322 == 'n')
	{
          Leu1++;
        }
        if(x7xxY322 == 'M' || x7xxY322 == 'm')
	{
          Leu4++;
          Thr++;
          Gly2++;
        }
        if(x7xxY322 == 'S' || x7xxY322 == 's')
	{
          Orn++;
          Lys++;
        }

        // analyze 330 residue
        if(x7xxY330 == 'F' || x7xxY330 == 'f')
	{
          Aad++;
        }
        if(x7xxY330 == 'V' || x7xxY330 == 'v')
	{
          Ala++;
          Asn++;
          Dab++;
          Dhb++;
          Gln++;
          Glu++;
          Ile++;
          Leu1++;
          Leu2++;
          Leu3++;
          Leu4++;
          Phe++;
          Pro++;
          Thr++;
          Tyr1++;
          Tyr2++;
          Tyr3++;
          Val2++;
          Val3++;
          Lys++;
        }
        if(x7xxY330 == 'I' || x7xxY330 == 'i')
	{
          Asp++;
          Cys++;
          Orn++;
          Ser++;
          Gly1++;
        }
        if(x7xxY330 == 'T' || x7xxY330 == 't')
	{
          Ile++;
          Val1++;
          Gly3++;
        }
        if(x7xxY330 == 'L' || x7xxY330 == 'l')
	{
          Phg++;
        }
        if(x7xxY330 == 'A' || x7xxY330 == 'a')
 	{
          Pip++;
        }
        if(x7xxY330 == 'M' || x7xxY330 == 'm')
	{
          Leu2++;
          Gly2++;
        }

        // analyze 331 residue
        if(x7xxY331 == 'V' || x7xxY331 == 'v')
	{
          Aad++;
          Leu1++;
          Leu3++;
          Pip++;
          Pro++;
          Tyr2++;
          His++;
        }
        if(x7xxY331 == 'L' || x7xxY331 == 'l')
	{
          Ala++;
          Val3++;
        }
        if(x7xxY331 == 'G' || x7xxY331 == 'g')
	{
          Asn++;
          Asp++;
        }
        if(x7xxY331 == 'T' || x7xxY331 == 't')
	{
          Cys++;
        }
        if(x7xxY331 == 'W' || x7xxY331 == 'w')
	{
          Cys++;
        }
        if(x7xxY331 == 'S' || x7xxY331 == 's')
	{
          Dab++;
        }
          if(x7xxY331 == 'N' || x7xxY331 == 'n')
	  {
            Dhb++;
          }
          if(x7xxY331 == 'D' || x7xxY331 == 'd')
	  {
            Gln++;
            Glu++;
            Orn++;
            Ser++;
	  }
          if(x7xxY331 == 'Y' || x7xxY331 == 'y')
	  {
            Ile++;
            Val2++;
          }
          if(x7xxY331 == 'F' || x7xxY331 == 'f')
	  {
            Ile++;
            Leu4++;
            Val1++;
	  }
          if(x7xxY331 == 'M' || x7xxY331 == 'm')
	  {
            Leu1++;
	  }
          if(x7xxY331 == 'C' || x7xxY331 == 'c')
	  {
            Phe++;
            Phg++;
            Tyr3++;
          }
          if(x7xxY331 == 'H' || x7xxY331 == 'h')
	  {
            Thr++;
          }
          if(x7xxY331 == 'A' || x7xxY331 == 'a')
	  {
            Tyr1++;
          }


        // determine most specific Gly
        Gly = Gly1;
        if(Gly2 > Gly)
        {
          Gly = Gly2;
        }
        if(Gly3 > Gly)
        {
          Gly = Gly3;
        }

        // determine most specific Tyr
        Tyr = Tyr1;
        if(Tyr2 > Tyr)
        {
          Tyr = Tyr2;
        }
        if(Tyr3 > Tyr)
        {
          Tyr = Tyr3;
        }

        // determine most specific Leu
        Leu = Leu1;
        if(Leu2 > Leu)
        {
          Leu = Leu2;
        }
        if(Leu3 > Leu)
        {
          Leu = Leu3;
        }

        // determine most specific Val
        Val = Val1;
        if(Val2 > Val)
        {
          Val = Val2;
        }
        if(Val3 > Val)
        {
          Val = Val3;
        }

        outputSpecificitiesNRPS( Aad, Ala, Asn, Asp, Cys, Dab, Dhb, Gln, Glu,
        	                Ile, Leu, Orn, Phe, Phg, Pip, Pro, Ser, Thr,
		                Tyr, His, Lys, Gly, Val);

        scoredSubstrate = maxSpecificityNRPS(Aad, Ala, Asn, Asp, Cys, Dab,
                                        Dhb, Gln, Glu, Ile, Leu, Orn,
                                        Phe, Phg, Pip, Pro, Ser, Thr,
					Tyr, His, Lys, Gly, Val,
                                        displayUnknown);
         clearPositions( pos235, pos236, pos239,
         pos278, pos299, pos301, pos322, pos330,
         pos331, pos58, pos59, pos60, pos61, pos62,
         pos70, pos72, pos74, pos75, pos77, pos93,
         pos189, pos191, pos198, pos199, pos200 );

       // cout << endl;

    Aad = 0;
    Ala = 0;
    Asn = 0;
    Asp = 0;
    Cys = 0;
    Dab = 0;
    Dhb = 0;
    Gln = 0;
    Glu = 0;
    Ile = 0;
    Leu = 0;
    Orn = 0;
    Phe = 0;
    Phg = 0;
    Pip = 0;
    Pro = 0;
    Ser = 0;
    Thr = 0;
    Tyr = 0;
    Val = 0;
    His = 0;
    Lys = 0;
    Gly = 0;
    Gly1 = 0;
    Gly2 = 0;
    Gly3 = 0;
    Leu1 = 0;
    Leu2 = 0;
    Leu3 = 0;
    Leu4 = 0;
    Tyr1 = 0;
    Tyr2 = 0;
    Tyr3 = 0;
    Val1 = 0;
    Val2 = 0;
    Val3 = 0;

       }
       foundSubstrate = 0;
         In.open("blast3.out");

       In >> segment;
       while( !In.fail())
       {
         if(segment == "*" && foundSubstrate == 0)
         {
	   In >> segment;
           predictedSubstrate = segment;
           foundSubstrate = 1;
         }
         In >> segment;
       }

       cout << "predicted substrate: " << predictedSubstrate << endl;
       cout << "scoredSubstrate: " << scoredSubstrate << endl;

       if(predictedSubstrate.length() == 0 && NRPS)
       {
         if(sigSeq.length() < minNrpSigSeqLength)
         {
           sufficientSigSeqLength = 0;
         }
         else
         {
           predictedSubstrate = scoredSubstrate;
         }
       }
       if(predictedSubstrate.length() == 0 && PKS)
       {
         if(sigSeq.length() < minPkSigSeqLength)
         {
           sufficientSigSeqLength = 0;
         }
         else
         {
           predictedSubstrate = scoredSubstrate;
         }
       }

       // reset
       sbjct.clear();
       query.clear();
       sigSeq.clear();
       In.close();

       // string to keep track of substrates
       predictedSubstrates.append(predictedSubstrate);
       predictedSubstrates.append(" ");

       if(sufficientSigSeqLength)
       {
         residue = predictedSubstrate; //set residue to predicted substrate
         predictedSubstrate.clear();
         fname = RESID_DATABASE;
         success = readDatabase(fname, residue, smileName); // search for residue
         // if database does not have SMILES of residue
       }
       sufficientSigSeqLength = 1;

       if ( !success )
       {
         quitLoop = true;
         validInput = false;
         inString = "done";
         cout << "Cannot find the SMILES string of " << residue << endl;
         cout << "No residue is saved" << endl;
       }
       else
       {
         // modify residues according to list of modifications

         //cout << "smileName.find[X] " << smileName.find("[X]") << endl;
         if(smileName.find("[X]") <= 0
            || smileName.find("[X]") > smileName.length()) // if a known substrate
         {
           // PKS modifications

         if(siteCount < sortedListOfDomains.getNumElems())
         {
           //cout << "siteCount: " << siteCount << endl;
           //cout << "siteCount-1 mass: "
               // << sortedListOfDomains.getElemAtIndex(siteCount-1).getMass() << endl;
           currentDomainPos = sortedListOfDomains.getElemAtIndex(siteCount-1).getMass();
           //cout << "siteCount mass: "
             //   << sortedListOfDomains.getElemAtIndex(siteCount).getMass() << endl;

           nextDomainPos = sortedListOfDomains.getElemAtIndex(siteCount).getMass();
           if( sortedListOfDomains.getNumElems() > siteCount + 1)
           {
             thirdDomainPos = sortedListOfDomains.getElemAtIndex(siteCount+1).getMass();
           }
           else
           {
             thirdDomainPos = currentDomainPos + modRange;
           }

           for(int i = 0; i < sortedListOfMods.getNumElems(); i++)
           {

             mod = sortedListOfMods.getElemAtIndex(i).getFormula();
             //cout << "mod: " << mod << endl;

             if(sortedListOfMods.getElemAtIndex(i).getMass() > nextDomainPos
                && sortedListOfMods.getElemAtIndex(i).getMass()
                   < thirdDomainPos)
             {
               if(mod[0] == 'K' && mod[1] == 'R' && KR == 0)
               {
                 KR = 1;
                 cout << "KR" << endl;
               }
               if(mod[0] == 'D' && DH == 0)
               {
                 DH = 1;
                 cout << "DH" << endl;
               }
               if(mod[0] == 'E' && ER == 0)
               {
                 ER = 1;
                 cout << "ER" << endl;
               }
               if(mod[0] == 'C' && mod[1] == 'M')
               {
                 HY = 1; // hydrogenated with methyl
               }
               if(KR && !DH && !ER && moddedAlready == 0)
               {
                 for(int i = smileName.length()-1; i > 0; i--)
                 {
                   if(smileName[i] == '=')
                   {
                     smileName.erase(i,1);
                   }
                 }
                 moddedAlready = 1;
               }
               if(KR && DH && !ER && moddedAlready == 0)
               {
                 //cout << "KR and DH" << endl;
                 smileName.erase(smileName.length()-4, 4);
                 if(!HY)
                 {
                   smileName.insert(smileName.length(), "=");
                 }
                 moddedAlready = 1;
               }
               if(KR && DH && ER && moddedAlready == 0)
               {
                 smileName.erase(smileName.length()-4, 4);
                 moddedAlready = 1;
               }
             } // PKS type modifications end

               // NRPS type modifications begin

              // basic N and C methylation
              if(sortedListOfMods.getElemAtIndex(i).getMass() > currentDomainPos
                && sortedListOfMods.getElemAtIndex(i).getMass() < nextDomainPos)
              {
                if(mod[0] == 'C' && mod[1] == 'M' && PKS)
                {
                  if(KR == 1 && DH == 1 && ER == 0)
                  {
                    smileName.erase(smileName.length()-1, 1);
                  }

                  cout << "C-METHYLATION" << endl;
                  for(int i = 0; i < smileName.length(); i++)
                  {
                    if(smileName[i] == 'C' && smileName[i+1] != '1')
                    {
                      cout << "SMILE: " << smileName << endl;
                      smileName.insert(i+1, "(C)");
                      cout << "SMILE: " << smileName << endl;
                      i = smileName.length();
                    }
                  }
                }
              if(mod[0] == 'N' && mod[1] == 'M' && NRPS)
                {
                  if(KR == 1 && DH == 1 && ER == 0)
                  {
                    smileName.erase(smileName.length()-1, 1);
                  }

                  cout << "N-METHYLATION" << endl;
                  for(int i = 0; i < smileName.length(); i++)
                  {
                    if(smileName[i] == 'N' && smileName[i+1] != '1')
                    {
                      cout << "SMILE: " << smileName << endl;
                      smileName.insert(i+1, "(C)");
                      cout << "SMILE: " << smileName << endl;
                      i = smileName.length();
                    }
                  }
                }
                if(mod[0] == 'C' && mod[1] == 'D')
                {
                  condCount++;
                  //cout << "CONDENSATION" << endl;
                }
                if(condCount == 2)
                {
                  condCount = 0;
                  cout << "EPIMERIZATION" << endl;
                  if(smileName.find("@@") > 0
                     && smileName.find("@@") < smileName.length())
                  {
                    smileName.erase(smileName.find("@@"), 1);
                  }
                }
              } // NRPS modifications end
           } // modifications end
           }
           KR = 0;
           DH = 0;
           ER = 0;
           HY = 0;
           moddedAlready = 0;
           condCount = 0;
         }
         if(siteCount == sortedListOfDomains.getNumElems())
         {
           //cout << "siteCountLast: " << siteCount << endl;
           currentDomainPos = sortedListOfDomains.getElemAtIndex(siteCount-1).getMass();

           for(int i = 0; i < sortedListOfMods.getNumElems(); i++)
           {
             mod = sortedListOfMods.getElemAtIndex(i).getFormula();
             //cout << "mod: " << mod << endl;

             if(sortedListOfMods.getElemAtIndex(i).getMass() > currentDomainPos
                && sortedListOfMods.getElemAtIndex(i).getMass()
                   < currentDomainPos + modRange)
             {

               if(mod[0] == 'C' && mod[1] == 'M' && PKS)
               {
                 if(KR == 1 && DH == 1 && ER == 0)
                 {
                   smileName.erase(smileName.length()-1, 1);
                 }
                 cout << "C-METHYLATION" << endl;
                 for(int i = 0; i < smileName.length(); i++)
                 {
                   if(smileName[i] == 'C' && smileName[i+1] != '1')
                   {
                     cout << "SMILE: " << smileName << endl;
                     smileName.insert(i+1, "(C)");
                     cout << "SMILE: " << smileName << endl;
                     i = smileName.length();
                   }
                 }
               }
               if(mod[0] == 'N' && mod[1] == 'M' && NRPS)
               {
                 cout << "N-METHYLATION" << endl;
                 for(int i = 0; i < smileName.length(); i++)
                 {
                   if(smileName[i] == 'N' && smileName[i+1] != '1')
                   {
                     cout << "SMILE: " << smileName << endl;
                     smileName.insert(i+1, "(C)");
                     cout << "SMILE: " << smileName << endl;
                     i = smileName.length();
                   }
                 }
               }
               if(mod[0] == 'C' && mod[1] == 'D')
               {
                 condCount++;
                 //cout << "CONDENSATION" << endl;
               }
               if(condCount == 2)
               {
                 condCount = 0;
                 cout << "EPIMERIZATION" << endl;
               }
             }
           }
         }
         cout << smileName << endl; // in SMILES database
         cout << "position: " << position << endl;
         cout << endl;
         aaName = new UnitClass;
         aaName->setUnit( residue, smileName );
         aaName->setMass(position);
         aaName->setFormula( "0" );
         aaNameList.enqueue( *aaName ); // add SMILES to chain
         delete aaName;
       }
       NRPS = 0;
       PKS = 0;
       mod.clear();
     }
   }
      In >> segment;
      whileCount++;
  }
    whileCount++;
  }


}

  sortedListOfDomains.clear();
  sortedListOfMods.clear();

}

// ask for NRPS residue number
int getIntFromUser( const string name )
{
  int  max;
  int  min;
  int inputNum;
  bool validInput = false;

  // define maximum and minimum type
  min = MIN_RESID_NUM;
  max = MAX_RESID_NUM;
  cout << "Enter the number of " << name << ": ";

  // error checking; only integer within limit is accepted
  while (!validInput)
  {
    cin >> inputNum;
    // check if input is of right type
    if ( cin.fail() )
    {
      cin.clear();
      cin.ignore(200, '\n');
      cout << "Invalid input type!" << endl;
      cout << "Enter number of " << name << ": ";
    }
    // check if input residue number is within limit
    else if ( inputNum >= min && inputNum <= max )
    {
      validInput = true;
    }
    else
    {
      cout << "Enter number of " << name
           << " is out of current supporting limit" << endl;
      cout << "Enter number of " << name << ": ";
      validInput = false;
    }
  }
  return( inputNum );
}


#include <vector>
vector<string> tokenize(const string str, const char token)
{
	vector<string> v;

	int oldi = 0;
	int i=str.find_first_of(token);
	while (i != -1)
	{
		v.push_back(str.substr(oldi, i-oldi));
		oldi = i + 1;
		i=str.find_first_of(token, i+1);
	}
	v.push_back(str.substr(oldi));
	return v;
}



// ask for NRPS residue/modification domain type and number
// and save the generated modified residue in aaNameList for post-reactions
bool inputData( string &termination,
                QueueClass< UnitClass > &aaNameList,
                string dnaFilename, string &predSubstrates,
                string displayUnknown,
                int nr_nrps_domains,
                string nrps_domains
              )
{
  termination = "TE"; // set TE as the default termination for now;
  bool readingSeq = 1;
  string DNASeq;
  string DNASubSeq;
  string inputSeq;
  string outputSeq; // reading frame 1
  int stopCount1;
  int stopCount2;
  int stopCount3;
  int stopCount;
  int start1;
  int start2;
  int start3;
  string menuChoice;
  string aminoAcidSeq;
  string nucleotideSeq;
  aminoAcidSeq = "mixedNRPSPKS.txt";
  bool validInput;
  bool quitLoop;
  string predictedSubstrates;
  string name = "NRPS domains";
  string inString;
  string residue;
  string fname;
  string smileName;
  int i = 0;
  int numResid;
  bool success;
  bool loop = true;
  bool allFramesRead = 0;
  UnitClass *aaName;
  ReactionClass reactions;
  int minDNAlength = 300;
  int minAAlength = 100;
  int Aad = 0;
  int Ala = 0;
  int Asn = 0;
  int Asp = 0;
  int Cys = 0;
  int Dab = 0;
  int Dhb = 0;
  int Gln = 0;
  int Glu = 0;
  int Ile = 0;
  int Leu = 0;
  int Orn = 0;
  int Phe = 0;
  int Phg = 0;
  int Pip = 0;
  int Pro = 0;
  int Ser = 0;
  int Thr = 0;
  int Tyr = 0;
  int Val = 0;
  int His = 0;
  int Lys = 0;
  int Gly = 0;
  ofstream substrates;

  /*
  cout << "1. Input new NRPS/PKS amino acid sequence." << endl;
  cout << "2. Input new NRPS/PKS nucleotide sequence." << endl;
  cout << "3. Input new NRPS/PKS substrate sequence." << endl;
  cout << "4. Return to main menu." << endl;
  cin >> menuChoice;
  while(menuChoice != "1" && menuChoice != "2" && menuChoice != "3"
     && menuChoice != "4")
  {
    cout << "Invalid selection." << endl;
    cout << "1. Input new NRPS/PKS amino acid sequence." << endl;
    cout << "2. Input new NRPS/PKS nucleotide sequence." << endl;
    cout << "3. Input new NRPS/PKS substrate sequence." << endl;
    cout << "4. Return to main menu." << endl;
    cin >> menuChoice;
  }
  */
  menuChoice = "3";

  while(menuChoice == "1" || menuChoice == "2" || menuChoice == "3")
  {
    string predictedSubstrate;
    string predictedSubstrates;

    if(menuChoice == "2") // search nucleotide sequence
    {
      DNASeq.clear();
      readingSeq = 1;
      // convert nucleotide to aminoAcidSeq and output into file
      // labeled nucleotideSeq
      //readInSeq(DNASeq, readingSeq);
      //if(DNASeq.length() > minDNAlength)
      //{
        //cout << "Input FASTA file name of nucleotide sequence:" << endl;
        //cin >> nucleotideSeq;
        nucleotideSeq = dnaFilename.c_str();

        for(int i = 0; i < 3; i++)
        {
          if(i == 2)
          {
            allFramesRead = 1;
          }
          translateSeq(nucleotideSeq, outputSeq, i);
          predictDomains(Aad, Ala, Asn, Asp, Cys, Dab, Dhb, Gln, Glu,
        	         Ile, Leu, Orn, Phe, Phg, Pip, Pro, Ser, Thr,
		         Tyr, His, Lys, Gly, Val, "aminoAcidSeq.txt",
                         predictedSubstrates, aaNameList, validInput,
                         quitLoop, allFramesRead, i, displayUnknown);
        }
      /*}
      else
      {
        cout << "Nucleotide sequence is too short." << endl;
        quitLoop = true;
      }*/
    }

    if(menuChoice == "1") // search amino acid sequence
    {
      aminoAcidSeq.clear();
      cout << "Input FASTA file name of amino acid sequence: " << endl;
      cin >> aminoAcidSeq;
      readingSeq = 1;

      //if(aminoAcidSeq.length() > minAAlength)
      //{
        predictDomains(Aad, Ala, Asn, Asp, Cys, Dab, Dhb, Gln, Glu,
        	        Ile, Leu, Orn, Phe, Phg, Pip, Pro, Ser, Thr,
		        Tyr, His, Lys, Gly, Val, aminoAcidSeq,
                        predictedSubstrates, aaNameList, validInput,
                        quitLoop, 1, i, displayUnknown);
      /*}
      else
      {
        cout << "Amino acid sequence is too short." << endl;
        quitLoop = true;
      }*/
    }

    if(menuChoice == "1" || menuChoice == "2")
    {
      cout << "The predicted NRPS sequence is as follows: " << endl;
      //predictedSubstrates = "gly asp his";
      cout << endl;
      cout << " " << predictedSubstrates << endl;
      cout << endl;
      substrates.open("substrates.txt");
      substrates << predictedSubstrates << endl;
      substrates.close();
      predSubstrates = predictedSubstrates;
      aaNameList.printForward();
      predictedSubstrates.append("TE");
    }
    if(menuChoice == "3") // add substrates manually
    {
      validInput = false;
      quitLoop = false;

  // number of NRPS residues
  numResid = nr_nrps_domains;
  if (nr_nrps_domains != -1)
  {
      numResid = nr_nrps_domains;
  }
  else
  {
  	numResid = getIntFromUser( name );
  }

  //cout << "Enter 3-letter code of " << name
  //     << " and end with TE or RE: " << endl;

  // loop back only when the new input is a residue
  vector<string> s = tokenize(nrps_domains, ' ');
  while ( !quitLoop && i < numResid )
  {
    // this input functions only the frist time
    if( loop )
    {
      if (nr_nrps_domains != -1)
      	{
        	//cout << "firstloop\n";
        	inString = s[i];
        }
      else
      	cin >> inString;
      if (  inString == "TE" && inString == "RE" )
      {
        cout << inString << " cannot be the first element" << endl;
        cin.clear();
        cin.ignore(200, '\n');
        cout << endl;
        quitLoop = true;
        validInput = true;
      }
      loop = false;
    }

    // if input is neither residue nor module, quit
    if ( RESIDUES.find( inString ) == string::npos &&
         MODULES.find( inString ) == string::npos )
    {
      cout << "Entered " << inString << " is not supported" << endl;
      cin.clear();
      cin.ignore(200, '\n');
      cout << endl;
      quitLoop = true;
      validInput = true;
    }
    // if input is residue, look for next input
    else if ( RESIDUES.find( inString ) != string::npos && i >= 1)
    {
      // get SMILES data of the residue from database
      residue = inString;
      fname = RESID_DATABASE;
      success = readDatabase( fname, residue, smileName );
        cout << smileName << endl;
      // if database readout fail
      if ( !success )
      {
        quitLoop = true;
        validInput = false;
        inString = "done";
        cout << "Cannot find the SMILES string of " << residue << endl;
        cout << "No residue is saved" << endl;
      }

      // take in input ( residue or module )
      if (i != 0)
        inString = s[i]; //cin >> inString;
      fname = INITI_DATABASE;
      success = readDatabase( fname, inString, smileName );
      // check if the input is an initiating-only residue, if yes, quit
      if ( success )
      {
        cin.clear();
        cin.ignore(200, '\n');
        quitLoop = true;
        validInput = false;
        cout << inString << " can only be used as the initiating group "
             << endl << "No residue is saved" << endl;
      }


      // while the next one is a module
      while ( !quitLoop &&
              RESIDUES.find( inString ) == string::npos &&
              MODULES.find( inString ) != string::npos )
      {
        // function to do reaction
        // will read in smileName and modification type
        if ( inString != "TE" && inString != "RE" )
        {
          smileName = reactions.preAssemblyReactions( smileName, inString );
        }

        // if module is an End module, save and quit
        if ( inString == "RE" || inString == "TE"
             || inString == "re" || inString == "te"
             || inString == "Re" || inString == "Te")
        {
          quitLoop = true;
          validInput = true;
          termination = inString;
          inString = "done";
          cin.clear();
          //cin.ignore(200, '\n');
        }
        // else get the new input
        else
        {
          // take in input ( residue only )
          cout << "testresonlyinput";
          inString = s[i]; //cin >> inString;
          fname = INITI_DATABASE;
          success = readDatabase( fname, inString, smileName );
          // check if the input is an initiating-only residue, if yes, quit
          if ( success )
          {
            cin.clear();
            cin.ignore(200, '\n');
            quitLoop = true;
            validInput = false;
            cout << inString << " can only be used as the initiating group "
                 << endl << "No residue is saved" << endl;
          }

          loop = false;
        }
      }


      // if next input is invalid, quit
      if ( RESIDUES.find( inString ) == string::npos &&
           MODULES.find( inString ) == string::npos &&
           inString != "done" )
      {
        cout << "Entered " << inString << " is not supported" << endl;
        cin.clear();
        cin.ignore(200, '\n');
        cout << endl;
        quitLoop = true;
        validInput = false;
      }
      // if next input is residue, save both residues
      else
      {
        if (i >= 1)
        {  // after reaction, output smileName and let aaNameList save it
          aaName = new UnitClass;
          aaName->setUnit( residue, smileName );
          aaName->setMass( 0 );
          aaName->setFormula( "0" );
          aaNameList.enqueue( *aaName );
          delete aaName;
	    }
      }

    }

    i++;
  }


  return( validInput );



    }
       /*
        cout << "1. Input NRPS amino acid sequence." << endl;
        cout << "2. Input NRPS nucleotide sequence." << endl;
        cout << "3. Input NRPS substrate sequence." << endl;
        cout << "4. Return to main menu." << endl;
        cin >> menuChoice;
        */
        menuChoice = "4";

 }

  if(menuChoice == "4")
  {
    cout << "returning to main menu..." << endl;
    quitLoop = true;
    validInput = true;
  }
  else
  {
    // delete aaNameList;
  }

  return( validInput );
}



// print out main menu and ask for input
int menuOption()
{
  int inputNum;
  bool validInput = false;

  // print out menu option
  for ( int i = 0; i < MENU_NUM; i++ )
  {
    cout << "  " << (i + 1) << ". " << MENU[i] << endl;
  }
  cout << "Enter menu option: ";

  while ( !validInput )
  {
    cin >> inputNum;
    // check input type
    if ( cin.fail() )
    {
      cin.clear();
      cin.ignore( 200, '\n' );
      cout << "Invalid input type!" << endl;
      cout << "Enter menu option: ";
      validInput = false;
    }
    // check input range
    else if ( inputNum < 1 || inputNum > MENU_NUM )
    {
      cout << "Entered number out of range" << endl;
      cout << "Enter menu option: ";
      validInput = false;
    }
    else
    {
      validInput = true;
    }
  }

  return( inputNum );
}

bool inputedPostMods( QueueClass< string > &postModifications )
{
  bool postModLoop = true;
  string postMod;
  bool inputedPostModifications = false;
  while(!inputedPostModifications) // ask for post-modifications
  {
    cout << "Input post-assembly modifications: (Signify end by typing END)"
         << endl;
    cin >> postMod;
    postModLoop = true;

    if( postMod == "END")
    {
      postModLoop = false;
      inputedPostModifications = true;
    }
    if(MODULES.find( postMod ) == string::npos && postMod != "END")
    {
      cout << "Entered " << postMod << " is not supported" << endl;
      cin.clear();
      cin.ignore(200, '\n');
      postModLoop = false;
    }
    while( postModLoop && !inputedPostModifications)
    {
      if( MODULES.find( postMod ) == string::npos) // invalid modification
      {
        cout << "Entered " << postMod << " is not supported" << endl;
        cin.clear();
        cin.ignore(200, '\n');
        postModLoop = false;
      }
      else if(MODULES.find( postMod ) != string::npos) //store modification
      {
        postModifications.enqueue(postMod);
      }
      if( postModLoop )
      {
        cin >> postMod; // request another modification
      }
      if( postMod == "END") // if user types "END", stop asking
      {
        postModLoop = false;
        inputedPostModifications = true;
      }
    }
  }
  return inputedPostModifications;
}


