#ifndef _REACTIONCLASS_H_
#define _REACTIONCLASS_H_

class ReactionClass 
{

  public:
    void addSmileToLibrary(const string &SMILE, QueueClass< UnitClass > &products);

    void tripleEster( const string &inPeptide, QueueClass< UnitClass > &products);
   
    string reverseSMILES( const string &forSMILES);

    void amideCyc( const string &inPeptide, QueueClass< UnitClass > &products);

    void esterCycDim( const string &inPeptide, QueueClass< UnitClass > &products, const string dimerize);
    
    int aminoAcidCounter( const string &inSMILE);

    string esterDimerize( const string &forSMILES, const string &smileToRev, string &linearEsterDimer);

    void disulfideBond( const string &inSMILES, QueueClass< UnitClass > &products, const int &mode);   
 
    void cyclization( const string inPeptide,
                       const string terminType,
                       QueueClass< UnitClass > &products,
                       const string dimerize );

    string preAssemblyReactions( const string smileName,
                                            const string inString);

    string epimerization( const string smileName );

    string N_Methylation( const string smileName );

    string O_Methylation( const string smileName );

    string S_Methylation( const string smileName );

    string hydroxylation( const string smileName );

    void postAssemblyReactions( QueueClass< string > &postModifications,
                                QueueClass< UnitClass > & postProducts,
                                QueueClass< UnitClass > &emptyLibrary);

    void postGlycosylation ( 
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary 
                            );

    void postOMethylation (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary
                           );

    void postNMethylation (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary
                          );

    void postSMethylation (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary
		      );

    void postHydroxylation (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary
                          );
    void postChlorination (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary
                          );
    void postBromination (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary
                          );
    void postIodination (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary
                          );

    void postCyclization (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary,
                      bool forw    
                      );

    void postEsterCyclization (
                      string tempPeptide,
                      int j,
                      QueueClass< UnitClass > &postCopy,
                      QueueClass< UnitClass > &peptideLibrary
                          );

};
#include "ReactionClass.inl"

#endif
