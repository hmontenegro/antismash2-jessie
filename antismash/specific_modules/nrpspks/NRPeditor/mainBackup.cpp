// EECS 498: Project 5, April 13, 2007
// Team: Peter Ung, Huan Tang Li, Satwikeshwar Palagiri
// Program: Nonribosomal Peptide Library Builder
// Description: This program takes in both common amino
// acids and noncommon substrates, and allows the user to
// modify each building block with typical chemical reactions
// such as methylation and hydroxylation. The user may 
// modify the protein through direct input or allow the
// program to build a library of products in the 
// post-modifications component.

#include <iostream>
#include <string>
using namespace std;

#include "constants.h"
#include "getUserInput.h"
#include "fileInputOutput.h"
#include "massAndFormula.h"
#include "QueueClass.h"
#include "ListNodeClass.h"
#include "UnitClass.h"
#include "ReactionClass.h"
#include "MSClass.h"
#include "deleteRepeat.h"


// overloading operator for outputing nodes
ostream& operator <<(ostream& outputStream, const UnitClass& domain );

ostream& operator <<(ostream& outputStream, const UnitClass& domain )
{
  if( domain.getMass() == 0 )
  {
    outputStream << domain.getUnit() << "  "
                 << domain.getSmile() << endl;
  }  
  else
  {
    outputStream << "    Formula:  "
                 << domain.getFormula() 
                 << "    Mass:  " 
                 << domain.getMass() << endl 
                 << " " << domain.getSmile() << endl;
  }

return outputStream;
}

int main(int argc, char* argv[])
{
  string residue[MAX_RESID_NUM];
  string module[MAX_MODUL_NUM];
  string reaction;
  string name;
  string fname;
  string formula;
  string peptide;
  string terminType;
  string tempPeptide;
  int numResid = 0;
  int choice;
  float MSMass[10];
  double mass;
  bool success;
  bool validInput;
  bool run = true;
  char deleteRpts;
  string predictedSubstrates;

  cout << "argc = " << argc << endl;
  for(int i = 0; i < argc; i++)
  cout << "argv[" << i << "] = " << argv[i] << endl;
   

  // List of amino acid residues
  QueueClass< UnitClass > *aaNameList;
  aaNameList = new QueueClass< UnitClass >;

  QueueClass< UnitClass > cyclized, uncyclized, modded;

  // List of modification modules
  UnitClass *unitName;
  QueueClass< UnitClass > *unitNameList;
  unitNameList = new QueueClass< UnitClass >;

  // List of assemblies of peptide
  QueueClass< UnitClass > *assemblyPepList;
  assemblyPepList = new QueueClass< UnitClass >;

  // List of post-assembly modifications of peptide
  QueueClass< UnitClass > *postAssemblyPepList;
  postAssemblyPepList = new QueueClass< UnitClass >;

  // Class to react NRPS molecules
  ReactionClass reactPeptide;;

  // Library of peptides after post-assembly mods
  QueueClass< UnitClass > *peptideLibrary;
  peptideLibrary = new QueueClass< UnitClass >;
  QueueClass< UnitClass > *cyclizedLibrary;
  cyclizedLibrary = new QueueClass< UnitClass >;
  QueueClass< UnitClass > *origPeptideLibrary;
  origPeptideLibrary = new QueueClass< UnitClass >;

  QueueClass< UnitClass > *msPrint;
  msPrint = new QueueClass< UnitClass >;
  QueueClass< string > postModifications;
  string dnaFilename;
  string aReaction = "AA";
  float minMass;
  float maxMass; 
  string outputFilename;

  if(argc == 1)
  {
    cout << "no file name supplied" << endl;
    return 0;
  } 
  if(argc > 1) // if a filename is typed
  {
    dnaFilename = argv[1]; 
  }
  if(argc == 2) // if a filename is typed but no limits on mass
  {
    minMass = 1;
    maxMass = 5000;
  }
  if(argc == 3) // if a min value is typed by not max
  {
    minMass = atof(argv[2]);
    if(minMass < 0 || minMass > 5000)
    { 
      minMass = 1;
    }
    maxMass = 5000; // arbitrary max mass
  }
  if(argc > 3) // if both min and max values are typed
  {
    minMass = atof(argv[2]);
    if(minMass < 0 || minMass > 5000)
    {
      minMass = 1;
    }
    maxMass = atof(argv[3]);
    if(maxMass < 0 || maxMass > 5000)
    {
      maxMass = 5000;
    }
  }
  if(argc > 4) // if further reactions are done, store them
  {
    aReaction = argv[4];
  }
  if(argc > 5) // if an output file is specified
  {
    outputFilename = argv[5];
    outputFilename.insert(0, "SMILES/");

  }
  else
  {
    //outputFilename = "SMILES/output.txt";
    outputFilename = argv[1];
    outputFilename.insert(0, "SMILES/");
  }

  // MS
  MSClass massSpect;
  UnitClass origPeptide;

  while ( run && argc > 1 )
  {
    cout << endl;
    //choice = menuOption();
    choice = INPUT_RES;
    
    // Input NRPS/PKS residues:
    if ( choice == INPUT_RES)
    {
      validInput = false;
      // ask for input residues
      while ( !validInput )
      { 
        aaNameList->clear();
        validInput = inputData( terminType, *aaNameList, dnaFilename, predictedSubstrates );
      }

      assemblyPepList->clear();
      if ( aaNameList->getNumElems() > 0 )
      {
        // generate the precursor peptide for modification
        peptide = aaNameList->preAssembly();
        cout << "core peptide: " << peptide << endl;
        cout << "termintype: " << terminType << endl;
        // termination of peptide
       cyclized.clear();
       uncyclized.clear();
       modded.clear();
       peptideLibrary->clear();
       reactPeptide.cyclization( peptide, terminType, *assemblyPepList );

       // copy all of the assemblyPepList into a modded index    
       for(int i = 0; i < assemblyPepList->getNumElems(); i++)
       {
         modded.enqueue(assemblyPepList->getElemAtIndex(i));
       }
     
       for ( int i = 0; i < assemblyPepList->getNumElems(); i++ )
       {
         float mass;
         string formula;
         tempPeptide = assemblyPepList->getElemAtIndex(i).getSmile();
         massAndFormula( tempPeptide, mass, formula );
         origPeptide.setUnit( "0", tempPeptide );
         origPeptide.setMass( mass );
         origPeptide.setFormula( formula );
         origPeptideLibrary->insertValue( origPeptide );
        }
      }
    }
    
    // Perform post-assembly modifications
    //if( choice == INPUT_POSTMODS )
    if(argc > 4)
    { 
      if(modded.getNumElems() > 0)
      {
      //inputedPostMods( postModifications );
      //cout << endl;
        
      postModifications.enqueue(argv[4]);
        reactPeptide.postAssemblyReactions( postModifications,
                                          modded, *peptideLibrary );  
   
        int libSize = peptideLibrary->getNumElems();

        for ( int j = 0; j < origPeptideLibrary->getNumElems(); j++ )
        {     
          peptideLibrary->insertValue( origPeptideLibrary->getElemAtIndex(j) );
        }


        deleteRepeat(*peptideLibrary);
        cout << "Library size: " << peptideLibrary->getNumElems() << endl;
      }
      else
      {
        cout << "No molecules in database. Please input molecule or exit." 
             << endl;
      }
 
    }

    //Filter by mass:
    //if(choice == INPUT_MS)
    //{
      if ( msPrint->getNumElems() > 0 )
      {
        msPrint->clear();
      }
      success = false;
      //if ( !success )
      //{
      success = massSpect.setRange(minMass, maxMass);
	//}
      if ( success )
      {
        if ( peptideLibrary->getNumElems() > 0 )
        {
          name = "MS filter";
          fname = outputFilename.c_str();
          massSpect.writeToFile( fname, *peptideLibrary, *msPrint, predictedSubstrates );
        }
        else
        {
          name = "MS filter";
          fname = outputFilename.c_str();
          massSpect.writeToFile( fname, *assemblyPepList, *msPrint, predictedSubstrates );
        }
      }
      //}
 
    // Print entered NRPS residues and modifications:
    if ( choice == PRINT_NRPS )
    {
      aaNameList->printForward();
    }

    // Generate possible NRPS now!
    if ( choice == GENERATE )
    {
       if(peptideLibrary->getNumElems() != 0)
       {
         peptideLibrary->printForward();
       }
       else
       {
          assemblyPepList->printForward();
       } 
    }

    // Output generated NRPS/PKS into file
    if ( choice == OUTPUT )
    {
      name = "generated NRPS library to be saved";
      fname = getStringFromUser( name );

      if(peptideLibrary->getNumElems() != 0)
      {
        writeToFile( fname, *peptideLibrary);
      }
      else
      {
        writeToFile( fname, *assemblyPepList);
      } 
     
    }

    run = false;

    // Exit
    if ( choice == EXIT )
    {
      run = false;
    }

  }
  cout << endl;
  return( 0 );
}
