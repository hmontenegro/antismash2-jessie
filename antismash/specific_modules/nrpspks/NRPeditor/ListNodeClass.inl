#include "ListNodeClass.h"

using namespace std;

template < class T >

ListNodeClass< T >::ListNodeClass( 
         ListNodeClass *inPrev,  //Address of node that comes before this one
         const T &inVal,//Value to be contained in this node
         ListNodeClass *inNext   //Address of node that comes after this one
         )
{
  
  prevNode = inPrev;
  nextNode = inNext;
  nodeVal = inVal;

}

template < class T >
//Returns the value stored within this node.
T ListNodeClass< T >::getValue(
         ) const
{

  return nodeVal;

}

template < class T >
//Returns the address of the node that follows this node.
ListNodeClass< T >* ListNodeClass< T >::getNext(
         ) const
{

  return nextNode;

}

template < class T >
//Returns the address of the node that comes before this node.
ListNodeClass< T >* ListNodeClass< T >::getPrev(
         ) const
{

  return prevNode;

} 

template < class T >
//Sets the object's previous node pointer to the value passed in.
void ListNodeClass< T >::setPreviousPointer( ListNodeClass< T > *newPrevPtr)
{
    prevNode = newPrevPtr;
} 

template < class T >
//This function DOES NOT modify "this" node.  Instead, it uses
//the pointers contained within this node to change the previous
//and next nodes so that they point to this node appropriately.
void ListNodeClass< T >::setBeforeAndAfterPointers(
         )
{
  if ( prevNode != NULL )
  {
    prevNode->nextNode = this;
  }

  if ( nextNode != NULL )
  { 
    nextNode->prevNode = this;
  }
}


