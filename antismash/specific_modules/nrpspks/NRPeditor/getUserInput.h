#ifndef _GETUSERINPUT_H_
#define _GETUSERINPUT_H_

#include "UnitClass.h"
#include "QueueClass.h"

string getStringFromUser( const string &label );

int getIntFromUser( const string name );

bool inputData( string &termination,
                QueueClass< UnitClass > &aaNameList,
                string dnaFilename,
                string &predSubstrates,
                string displayUnknown
              );

int menuOption();

bool inputedPostMods( QueueClass< string > &postModifications );


#include "getUserInput.inl"

#endif
