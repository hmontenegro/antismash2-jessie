#include <sys/types.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/param.h>
#include <sys/wait.h>

# define ARG_MAX 131072

int spawnl(int mode, const char *path, ...) {
  char *argv[ARG_MAX];
  va_list args;
  pid_t pid;
  int status;
  int i;

  va_start(args, path);
  for (i = 0; i < ARG_MAX; i++) {
    argv[i] = va_arg(args, char *);
    if(!argv[i]) { break; }
  }
  va_end(args);

  if (i == ARG_MAX && argv[i-1]) { return -1; }

  pid = fork();
  if (pid == 0) {
    execv(path, argv);
    _exit(1);
  } else if (pid > 0) {
    if (waitpid(pid, &status, 0) == -1)
      return -1;
    return status;
  } else {
    return -1;
  }

  return -1;
}

