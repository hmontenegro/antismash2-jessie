# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2010-2012 Marnix H. Medema
# University of Groningen
# Department of Microbial Physiology / Groningen Bioinformatics Centre
#
# Copyright (C) 2011,2012 Kai Blin
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.

import logging
from os import path
from pyquery import PyQuery as pq
from antismash import utils

def will_handle(product):
    if product.find('nrps') > -1:
        return True
    if product.find('t1pks') > -1:
        return True
    if product.find('transatpks') > -1:
        return True

    return False

def generate_details_div(cluster, seq_record, options, js_domains, details=None):
    """Generate details div"""

    cluster_rec = utils.get_cluster_by_nr(seq_record, cluster['idx'])
    if cluster_rec is None:
        return details

    if details is None:
        details = pq('<div>')
        details.addClass('details')

        header = pq('<h3>')
        header.text('Detailed annotation')
        details.append(header)

    js_cluster_domains = { 'id': "cluster-%s-details" % cluster['idx'], 'orfs': [] }
    features = utils.get_cluster_cds_features(cluster_rec, seq_record)
    for feature in features:
        if not 'sec_met' in feature.qualifiers:
            continue

        if 'translation' in feature.qualifiers:
            sequence = feature.qualifiers['translation'][0]
        else:
            sequence = str(utils.get_aa_sequence(seq_record, feature))

        js_orf = {
            'id': utils.get_gene_id(feature),
            'sequence': sequence,
            'domains': [],
        }

        for qual in feature.qualifiers['sec_met']:
            if not qual.startswith('NRPS/PKS Domain:'):
                continue

            js_domain = _parse_domain(qual, feature, seq_record)
            if len(js_domain) > 0:
                js_orf['domains'].append(js_domain)

        if len(js_orf['domains']) > 0:
            js_cluster_domains['orfs'].append(js_orf)

    if len(js_cluster_domains['orfs']) > 0:
        details_svg = pq('<div>')
        details_svg.addClass('details-svg')
        details_svg.attr('id', '%s-svg' % js_cluster_domains['id'])
        details.append(details_svg)

        js_domains.append(js_cluster_domains)

    return details


def generate_sidepanel(cluster, seq_record, options, sidepanel=None):
    """Generate sidepanel div"""
    cluster_rec = utils.get_cluster_by_nr(seq_record, cluster['idx'])
    if cluster_rec is None:
        return sidepanel

    if sidepanel is None:
        sidepanel = pq('<div>')
        sidepanel.addClass('sidepanel')

    structure = pq('<div>')
    structure.addClass('structure')
    structure_header = pq('<h3>')
    structure_header.text('Predicted core structure')
    structure.append(structure_header)
    a = pq('<a>')
    a.attr('href', _get_structure_image_url(cluster_rec, options.outputfoldername))
    a.attr('target', '_new')
    structure.append(a)
    structure_img = pq('<img>')
    structure_img.attr('src', _get_structure_image_url(cluster_rec, options.outputfoldername))
    a.append(structure_img)
    warning = pq('<div>')
    warning.addClass('as-structure-warning')
    if "docking" in options and cluster['idx'] in options.docking and options.docking[cluster['idx']]:
        warning.text('Rough prediction of core scaffold based on assumed '
                    'PKS linker matching; tailoring reactions not taken '
                    'into account')
    else:
        warning.text('Rough prediction of core scaffold based on assumed '
                    'PKS/NRPS colinearity; tailoring reactions not taken '
                    'into account')
    structure.append(warning)
    sidepanel.append(structure)

    details = pq('<div>')
    details.addClass('more-details')
    details_header = pq('<h3>')
    details_header.text('Prediction details')
    details.append(details_header)
    details_list = pq('<dl>')
    details_list.addClass('prediction-text')

    details.append(details_list)
    sidepanel.append(details)
    dt = pq('<dt>')
    dt.text('Monomers prediction:')
    details_list.append(dt)
    dd = pq('<dd>')
    dd.text(_get_monomer_prediction(cluster_rec))
    details_list.append(dd)

    features = utils.get_cluster_cds_features(cluster_rec, seq_record)
    for feature in features:
        if not 'sec_met' in feature.qualifiers:
            continue

        header_printed = False
        for qual in feature.qualifiers['sec_met']:
            if not qual.startswith('NRPS/PKS Domain:'):
                continue

            preds = _parse_substrate_predictions(qual)
            for key, val in preds:
                if not header_printed:
                    dt = pq('<dt>')
                    dt.text(utils.get_gene_id(feature))
                    details_list.append(dt)
                    header_printed = True
                dd = pq('<dd>')
                dd.html('%s: %s<br>' % (key, val))
                details_list.append(dd)

            if len(preds) > 0:
                dd = pq('<dd>')
                dd.append(pq('<br>'))
                details_list.append(dd)

    if cluster['type'].find('nrps') > -1:
        cross_refs = pq("<div>")
        refs_header = pq('<h3>')
        refs_header.text('Database cross-links')
        cross_refs.append(refs_header)
        links = pq("<div>")
        links.addClass('prediction-text')

        a = pq("<a>")
        a.attr('href', 'http://bioinfo.lifl.fr/norine/form2.jsp')
        a.attr('target', '_new')
        a.text("Look up in NORINE database")
        links.append(a)
        cross_refs.append(links)
        sidepanel.append(cross_refs)

    return sidepanel


def _parse_substrate_predictions(domain):
    "Parse the substrate predictions from the NRPS/PKS domain string"
    predictions = []


    idx = domain.find('Substrate specificity predictions:')
    if idx == -1:
        return []

    specifities = domain[idx + 35:].strip(';').split(', ')

    for spec in specifities:
        try:
            spec = spec.strip()
            substrate, name = spec.split(' ', 1)
            name = name.strip('()')
            predictions.append((name, substrate))
        except ValueError:
            logging.debug(domain)
            logging.debug(spec)
            raise

    return predictions

def _get_structure_image_url(feature, outputfolder):
    "Get the relative url to the structure image"
    for note in feature.qualifiers.get('note', []):
        if not note.startswith("Structure image:"):
            continue

        url = note.split()[-1]
        if not path.exists(path.join(outputfolder, url)):
            logging.debug('No file at %r' % url)
            continue
        return url
    return 'images/nostructure_icon.png'

def _get_monomer_prediction(feature):
    "Get the monomer prediction of the cluster"
    for note in feature.qualifiers.get('note', []):
        if not note.startswith("Monomers prediction:"):
            continue

        monomers = note.split(':')[-1].strip()
        return monomers
    return 'N/A'

def _parse_domain(domain, feature, seq_record):
    "Convert a NRPS/PKS domain string to a dict useable by json.dumps"
    text = domain[17:]
    type_, location, prediction_string = text.split(' ', 2)
    predictions = _parse_substrate_predictions(prediction_string)

    location = location.strip('().')
    coordinates = location.split('-')

    #Create url_link to NaPDoS for C and KS domains
    napdoslink = ""
    domainseq = str(utils.get_aa_sequence(seq_record, feature))[int(coordinates[0]):int(coordinates[-1])]
    if "PKS_KS" in text:
        napdoslink = "http://napdos.ucsd.edu/cgi-bin/process_request.cgi?query_type=aa&amp;ref_seq_file=all_KS_public_12062011.faa&amp;Sequence=%3EKS_domain_from_antiSMASH%0D" + domainseq
    elif "Condensation" in text:
        napdoslink = "http://napdos.ucsd.edu/cgi-bin/process_request.cgi?query_type=aa&amp;ref_seq_file=all_C_public_12062011.faa&amp;Sequence=%3EC_domain_from_antiSMASH%0D" + domainseq
    blastlink = "http://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE=Proteins&amp;PROGRAM=blastp&amp;BLAST_PROGRAMS=blastp&amp;QUERY=" + domainseq + "&amp;LINK_LOC=protein&amp;PAGE_TYPE=BlastSearch"

    try:
        js_domain = { 'type': type_, 'start': int(coordinates[0]), 'end': int(coordinates[1]),
                      'predictions': predictions, 'napdoslink': napdoslink, 'blastlink': blastlink,
                      'sequence': domainseq }
        return js_domain
    except ValueError:
        logging.debug('%r' % text)
        logging.debug('%r  %r' % (type_, location))
        logging.debug(coordinates)
        raise
