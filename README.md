antiSMASH
=========

[![Build Status](https://drone.io/bitbucket.org/antismash/antismash2/status.png)](https://drone.io/bitbucket.org/antismash/antismash2/latest)

antiSMASH allows the rapid genome-wide identification, annotation and analysis
of secondary metabolite biosynthesis gene clusters in bacterial and fungal
genomes. It integrates and cross-links with a large number of in silico
secondary metabolite analysis tools that have been published earlier.

Technology Foundation
---------------------

antiSMASH is powered by several open source tools: BioPython, NCBI BLAST+,
HMMer 2 and 3, Muscle 3, Glimmer 3, FastTree, TreeGraph 2, Indigo-depict, PySVG
and JQuery SVG.

Development
-----------

antiSMASH is the product of a collaborative effort between the Department
of Microbial Physiology and Groningen Bioinformatics Centre of the University
of Groningen, the Department of Microbiology of the University of Tübingen, and
the Department of Bioengineering and Therapeutic Sciences at the University of
California, San Francisco.

Funding
-------

antiSMASH is supported by the GenBiotics program of the Dutch Technology
Foundation (STW), which is the applied-science division of The Netherlands
Organisation for Scientific Research (NWO) and the Technology Programme of the
Ministry of Economic Affairs (grant STW 10463) as well as the GenBioCom program
of the German Ministry of Education and Research (BMBF) grant 0315585A.

Publications
------------

Kai Blin, Marnix H. Medema, Daniyal Kazempour, Michael A. Fischbach, Rainer
Breitling, Eriko Takano, & Tilmann Weber (2013): antiSMASH 2.0 — a versatile
platform for genome mining of secondary metabolite producers. Nucleic Acids
Research 41: W204-W212.

Marnix H. Medema, Kai Blin, Peter Cimermancic, Victor de Jager, Piotr
Zakrzewski, Michael A. Fischbach, Tilmann Weber, Rainer Breitling & Eriko
Takano (2011). antiSMASH: Rapid identification, annotation and analysis of
secondary metabolite biosynthesis gene clusters. Nucleic Acids Research 39:
W339-W346.
