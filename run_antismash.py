#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
#
# Copyright (C) 2010-2012 Marnix H. Medema
# University of Groningen
# Department of Microbial Physiology / Groningen Bioinformatics Centre
#
# Copyright (C) 2011,2012 Kai Blin
# University of Tuebingen
# Interfaculty Institute of Microbiology and Infection Medicine
# Div. of Microbiology/Biotechnology
#
# License: GNU Affero General Public License v3 or later
# A copy of GNU AGPL v3 should have been included in this software package in LICENSE.txt.
"""Run the antiSMASH pipeline"""

import sys
import os

if sys.platform ==  ('win32') or sys.platform == ('darwin'):
    os.environ['EXEC'] = os.getcwd() + os.sep + "exec"
    os.environ['PYTHON'] = os.getcwd() + os.sep + "python"
    sys.path.append(os.sep.join([os.getcwd(), "python", "Lib", "site-packages"]))
    os.environ['PATH'] = os.pathsep + os.environ['PYTHON'] + os.pathsep + os.environ['PATH']
    os.environ['PATH'] = os.pathsep + os.environ['EXEC'] + os.pathsep + os.environ['PATH']

import logging
import argparse
from os import path
import multiprocessing
import straight.plugin
from helperlibs.bio import seqio
from antismash.config import load_config, set_config
from antismash import generic_modules
from antismash import utils
from antismash.generic_modules import hmm_detection, genefinding, fullhmmer, clusterfinder, smcogs, clusterblast, subclusterblast
from numpy import array_split, array
import urllib2
from urllib2 import Request,urlopen,URLError,HTTPError
import httplib
from httplib import BadStatusLine,HTTPException
import time
from collections import defaultdict
from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.Alphabet import ProteinAlphabet, NucleotideAlphabet
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

def ValidateClusterTypes(clustertypes):
    class Validator(argparse.Action):
        def __call__(self, parser, args, values, option_string = None):
            values = values.replace(";",",").split(",")
            try:
                for value in values:
                    if value not in clustertypes:
                        raise ValueError('invalid clustertype {s!r}'.format(s = value))
            except ValueError as e:
                print "\nInput error:", e, "\n"
                print "Please choose from the following list:\n", "\n".join(clustertypes), "\n\nExample: --enable t1pks,nrps,other"
                sys.exit(1)
            setattr(args, self.dest, values)
    return Validator


def main():
    multiprocessing.freeze_support()
    "Actually run the pipeline"

    # First load the output plugins so we can present appropriate options
    output_plugins = load_output_plugins()

    clustertypes = hmm_detection.get_supported_cluster_types()

    parser = argparse.ArgumentParser(description='Run the antiSMASH pipeline')
    parser.add_argument('sequences', metavar='sequence', nargs="*",
                        help="GenBank/EMBL/FASTA file(s) containing DNA")

    parser.add_argument('-d', '--debug', dest='debug',
                        action='store_true', default=False,
                        help="Print debugging information to stderr")
    parser.add_argument('--list-plugins', dest='list_plugins',
                        action='store_true', default=False,
                        help="List all available sec. met. detection modules")

    parser.add_argument('--check-prereqs', dest='check_prereqs_only',
                        action='store_true', default=False,
                        help="Just check if all prerequisites are met")
    parser.add_argument('-v', '--verbose', dest='verbose',
                        action='store_true', default=False,
                        help="Print verbose status information to stderr")
    parser.add_argument('-c', '--cpus', dest='cpus',
                        type=int, default=multiprocessing.cpu_count(),
                        help="How many CPUs to use in parallel (default: %s)" %
                        multiprocessing.cpu_count())
    parser.add_argument('--logfile', dest='logfile',
                        default=argparse.SUPPRESS,
                        help="Also write logging output to a file")
    parser.add_argument('--statusfile', dest='statusfile',
                        default=argparse.SUPPRESS,
                        help="Write the current status to a file")

    group = parser.add_argument_group('Analysis options')
    group.add_argument('--enable', dest='enabled_cluster_types',
                        action=ValidateClusterTypes(clustertypes), metavar="TYPES", default=clustertypes,
                        help="Select sec.met. cluster types to search for (default: all)")
    group.add_argument('--input-type', dest='input_type',
                        default='nucl', choices=['nucl', 'prot'],
                        help="Determine input type: amino acid sequence(s) or nucleotide sequence(s).")
    group.add_argument('--clusterblast', dest='clusterblast',
                        action='store_true', default=False,
                        help="BLAST identified clusters against known clusters")
    group.add_argument('--subclusterblast', dest='subclusterblast',
                        action='store_true', default=False,
                        help="BLAST identified clusters against known subclusters")
    group.add_argument('--smcogs', dest='smcogs',
                        action='store_true', default=False,
                        help="Look for sec.met. clusters of orthologous groups")
    group.add_argument('--inclusive', dest='inclusive',
                        action='store_true', default=False,
                        help="Use inclusive algorithm for cluster detection")
    group.add_argument('--full-hmmer', dest='full_hmmer',
                        action='store_true', default=False,
                        help="Run a whole-genome HMMer analysis")
    group.add_argument('--full-blast', dest='full_blast',
                        action='store_true', default=False,
                        help="Run a whole-genome BLAST analysis")
    group.add_argument('--from', dest='start',
                        type=int, default=-1,
                        help="Start analysis at nucleotide specified")
    group.add_argument('--to', dest='end',
                        type=int, default=-1,
                        help="End analysis at nucleotide specified")

    group = parser.add_argument_group('Output options')
    for plugin in output_plugins:
        group.add_argument('--disable-%s' % plugin.name, dest=plugin.name,
                           action='store_false', default=argparse.SUPPRESS,
                           help="Disable %s" % plugin.short_description)

    group = parser.add_argument_group('Gene finding options (ignored when ORFs are annotated)')
    group.add_argument('--eukaryotic', dest='eukaryotic',
                        action='store_true', default=False,
                        help="DNA is of eukaryotic origin")
    group.add_argument('--all-orfs', dest='all_orfs',
                        action='store_true', default=False,
                        help="Use all ORFs > 60 nt instead of running gene finding")

    group = parser.add_argument_group('Settings')
    group.add_argument('--pfamdir', dest='pfamdir', default=argparse.SUPPRESS,
                        help="Directory the Pfam-A.hmm file is located in")
    group.add_argument('--outputfolder', dest='outputfoldername',
                        default=argparse.SUPPRESS,
                        help="Directory to write results to")
    #Parse arguments, removing hyphens from the beginning of file names to avoid conflicts with argparse
    infile_extensions = ('.fasta', '.fas', '.fa', '.gb', '.gbk', '.emb', '.embl')
    sys.argv = [arg.replace("-","< > HYPHEN < >") if (arg.endswith(infile_extensions) and arg[0] == "-") else arg for arg in sys.argv]
    options = parser.parse_args()
    options.sequences = [filename.replace("< > HYPHEN < >","-") for filename in options.sequences]

    # Logging is useful for all the following code, so make sure that is set up
    # right after parsing the arguments.
    setup_logging(options)

    if options.input_type == 'prot' and (options.clusterblast or options.subclusterblast or options.smcogs or options.inclusive or options.full_hmmer or options.full_blast):
        logging.error("Protein input option is not compatible with --clusterblast, --subclusterblast, --smcogs, --inclusive, --full-hmmer, and --full-blast.")
        sys.exit(2)
    if options.input_type == 'prot' and (options.start != -1 or options.end != -1):
        logging.error("Protein input option is not compatible with --start and --end.")
        sys.exit(2)

    #Load configuration data from config file
    load_config(options)
    set_config(options)

    #Load and filter plugins
    utils.log_status("Loading detection plugins")
    plugins = load_detection_plugins()
    if options.list_plugins:
        list_available_plugins(plugins, output_plugins)
        sys.exit(0)
    filter_plugins(plugins, options, clustertypes)
    filter_outputs(output_plugins, options)

    #Check prerequisites
    if check_prereqs(plugins, options) > 0:
        logging.error("Not all prerequisites met")
        sys.exit(1)
    if options.check_prereqs_only:
        logging.info("All prerequisites are met")
        sys.exit(0)
    if not options.sequences:
        parser.error("Please specify at least one sequence file")

    if not 'outputfoldername' in options:
        options.outputfoldername = path.splitext(path.basename(options.sequences[0]))[0]
    if not os.path.exists(options.outputfoldername):
        os.mkdir(options.outputfoldername)
    options.full_outputfolder_path = path.abspath(options.outputfoldername)

    #Parse input sequence
    try:
        utils.log_status("Parsing the input sequence(s)")
        seq_records = parse_input_sequences(options)
        seq_records = [record for record in seq_records if len(record.seq) > 1000]
        if len(seq_records) == 0:
            logging.error("Input does not contain contigs larger than minimum size of 1000 bp.")
            sys.exit(1)
    except IOError as e:
        logging.error(str(e))
        sys.exit(1)

    if len(seq_records) < 1:
        logging.error("Sequence file is incorrectly formatted or contains no sequences of sufficient quality.")
        sys.exit(1)

    options.record_idx = 1
    for seq_record in seq_records:
        utils.log_status("Analyzing record %d" % options.record_idx)
        logging.info("Analyzing record %d" % options.record_idx)
        utils.sort_features(seq_record)
        strip_record(seq_record)
        utils.fix_record_name_id(seq_record)
        run_analyses(seq_record, options, plugins)
        utils.sort_features(seq_record)
        options.record_idx += 1

    #Write results
    options.plugins = plugins
    utils.log_status("Writing the output files")
    write_results(output_plugins, seq_records, options)
    zip_results(seq_records, options)

def strip_record(seq_record):
    features = utils.get_cds_features(seq_record)
    for feature in features:
        if feature.qualifiers.has_key('sec_met'):
            del feature.qualifiers['sec_met']
    clusters = utils.get_cluster_features(seq_record)
    deletefeatures = []
    for f in seq_record.features:
        if f in clusters:
            deletefeatures.append(seq_record.features.index(f))
    deletefeatures.reverse()
    for featurenr in deletefeatures:
        del seq_record.features[featurenr]


def run_analyses(seq_record, options, plugins):
    "Run antiSMASH analyses for a single SeqRecord"

    if 'next_clusternr' not in options:
        options.next_clusternr = 1

    options.clusternr_offset = options.next_clusternr

    #Detect gene clusters
    detect_geneclusters(seq_record, options)

    for f in utils.get_cluster_features(seq_record):
        logging.debug(f)

    #Do specific analyses
    # TODO: Run this in parallel, perhaps?
    cluster_specific_analysis(plugins, seq_record, options)
    unspecific_analysis(seq_record, options)

    #Run smCOG analysis
    if options.smcogs:
        utils.log_status("Detecting smCOGs for contig #%d" % options.record_idx)
        smcogs.run_smcog_analysis(seq_record, options)

    #Run ClusterBlast
    if options.clusterblast:
        utils.log_status("ClusterBlast analysis for contig #%d" % options.record_idx)
        clusterblast.run_clusterblast(seq_record, options)
        #clusterblastvars info could also be pickled are transferred some other way if we want it to be possible to reconstruct complete output from files

    #Run SubClusterBlast
    if options.subclusterblast:
        utils.log_status("SubclusterBlast analysis for contig #%d" % options.record_idx)
        subclusterblast.run_subclusterblast(seq_record, options)


def list_available_plugins(plugins, output_plugins):
    print("Support for detecting the following secondary metabolites:")
    for plugin in plugins:
        print(" * %s" % plugin.short_description)

    print("Support for the following output formats:")
    for plugin in output_plugins:
        print(" * %s" % plugin.short_description)


def filter_plugins(plugins, options, clustertypes):
    if options.enabled_cluster_types is None or options.enabled_cluster_types == clustertypes:
        return

    for plugin in plugins:
        if plugin.name in clustertypes and plugin.name not in options.enabled_cluster_types:
            plugins.remove(plugin)

    if plugins == []:
        print("No plugins enabled, use --list-plugins to show available plugins")
        sys.exit(1)


def filter_outputs(plugins, options):
    for plugin in plugins:
        if plugin.name in options:
            logging.debug("Removing plugin %r" % plugin.name)
            plugins.remove(plugin)

    if plugins == []:
        print("No plugins enabled, use --list-plugins to show available plugins")
        sys.exit(1)


def write_results(plugins, seq_records, options):
    for plugin in plugins:
        plugin.write(seq_records, options)


def zip_results(seq_records, options):
    "Create a zip archive with all the results generated so far"
    zip_name = '%s.zip' % seq_records[0].id
    utils.zip(path.abspath(options.outputfoldername), zip_name)


def setup_logging(options):
    "Set up the logging output"
    if options.debug:
        log_level = logging.DEBUG
    elif options.verbose:
        log_level = logging.INFO
    else:
        log_level = logging.WARNING

    logging.basicConfig(format='%(levelname)s: %(message)s',
                        level=log_level)
    if 'logfile' in options:
        if not os.path.exists(path.dirname(options.logfile)):
            os.mkdir(path.dirname(options.logfile))
        fh = logging.FileHandler(options.logfile)
        fh.setLevel(logging.INFO)
        fh.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
        logging.getLogger('').addHandler(fh)


def load_detection_plugins():
    "Load available secondary metabolite detection modules"
    logging.info('Loading detection modules')
    return straight.plugin.load('antismash.specific_modules')


def load_output_plugins():
    "Load available output formats"
    plugins = list(straight.plugin.load('antismash.output_modules'))
    plugins.sort(cmp=lambda x, y: cmp(x.priority, y.priority))
    return plugins


def fetch_entries_from_ncbi(efetch_url):
    urltry = "n"
    nrtries = 0
    output = ""
    while urltry == "n" and nrtries < 4:
        try:
            nrtries += 1
            time.sleep(3)
            req = urllib2.Request(efetch_url)
            response = urllib2.urlopen(req)
            output = response.read()
            if len(output) > 5:
                urltry = "y"
        except (IOError,httplib.BadStatusLine,URLError,httplib.HTTPException):
            logging.error("Entry fetching from NCBI failed. Waiting for connection...")
            time.sleep(5)
    return output


def fix_wgs_master_record(seq_record):
    updated_seq_records = []
    #If seq_record is a WGS master record, parse out contig accession numbers and download these as separate seq_records
    if seq_record.annotations.has_key('wgs_scafld'):
        contigranges = seq_record.annotations['wgs_scafld']
    else:
        contigranges = [seq_record.annotations['wgs']]
    allcontigs = []
    for contigrange in contigranges:
        if len(contigrange) == 1:
            allcontigs.extend(contigrange)
            continue
        startnumber, endnumber = '', ''
        alpha_tag = ''
        for char in contigrange[0].partition(".")[0]:
            if char.isdigit():
                startnumber += char
            else:
                alpha_tag += char
        for char in contigrange[1].partition(".")[0]:
            if char.isdigit():
                endnumber += char
        nrzeros = 0
        for char in startnumber:
            if char == "0":
                nrzeros += 1
            else:
                break
        contigrange = [alpha_tag + nrzeros * "0" + str(number) for number in xrange(int(startnumber), int(endnumber))]
        allcontigs.extend(contigrange)
    #Create contig groups of 50 (reasonable download size per download)
    nr_groups = len(allcontigs) / 50 + 1
    contig_groups = [list(np_array) for np_array in array_split(array(allcontigs), nr_groups)]
    #Return unchanged if no contigs supplied
    if len(contig_groups[0]) == 0 or (len(contig_groups[0]) == 1 and contig_groups[0][0] == ""):
        return [seq_record]
    #Download contigs and parse into seq_record objects
    for contig_group in contig_groups:
        efetch_url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&id='
        efetch_url = efetch_url + ",".join(contig_group) + '&rettype=gbwithparts&retmode=text'
        output = fetch_entries_from_ncbi(efetch_url)
        if not len(output) > 5:
            break
        if "Resource temporarily unavailable" in output[:200] or "<h1>Server Error</h1>" in output[:500] or "NCBI - WWW Error" in output[:500]:
            logging.error('ERROR: NCBI server temporarily unavailable: downloading contigs failed.')
            sys.exit(1)
        try:
            handle = StringIO(output)
            updated_seq_records.extend(list(SeqIO.parse(handle, 'genbank')))
        except ValueError, e:
            logging.error('Parsing %r failed: %s' % ("temporary contig file", e))
    return updated_seq_records


def fix_supercontig_record(seq_record):
    updated_seq_records = []
    #If seq_record is a supercontig record, reconstruct sequence and replace CONTIG feature by ORIGIN feature
    contig_info = seq_record.annotations['contig'].replace("join(","")
    if contig_info[-1] == ")":
        contig_info = contig_info[:-1]
    allcontigparts = contig_info.split(",")
    accessions = [part.partition(":")[0].partition(".")[0].rpartition("complement(")[2].replace(")","") for part in contig_info.split(",") if "gap(" not in part]
    #Create contig groups of 50 (reasonable download size per download)
    nr_groups = len(accessions) / 50 + 1
    contig_groups = [list(np_array) for np_array in array_split(array(accessions), nr_groups)]
    #Return unchanged if no contigs supplied
    if len(contig_groups[0]) == 0 or (len(contig_groups[0]) == 1 and contig_groups[0][0] == ""):
        return [seq_record]
    #Download contig sequences based on their accessions
    contigseqdict = {}
    for contig_group in contig_groups:
        efetch_url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&id='
        efetch_url = efetch_url + ",".join(contig_group) + '&rettype=fasta&retmode=text'
        output = fetch_entries_from_ncbi(efetch_url)
        if not len(output) > 5:
            break
        if "Resource temporarily unavailable" in output[:200] or "<h1>Server Error</h1>" in output[:500]:
            logging.error('ERROR: NCBI server temporarily unavailable: downloading contigs failed.')
            sys.exit(1)
        sequences = [seq for seq in output.split(">") if len(seq) > 5]
        for sequence in sequences:
            for contig_acc in contig_group:
                if contig_acc in sequence.partition("\n")[0]:
                    contigseqdict[contig_acc] = sequence.partition("\n")[2].replace("\n","")
    #Reconstruct supercontig sequence based on contig sequences and gaps
    fullsequence = ''
    for part in allcontigparts:
        if "gap(" in part:
            candidate_gap_int = part.partition('gap(')[2][:-1]
            if "unk" in candidate_gap_int:
                candidate_gap_int = candidate_gap_int.partition("unk")[2]
            if candidate_gap_int.isdigit():
                fullsequence += int(candidate_gap_int) * 'N'
            else:
                logging.error('Parsing supercontig file failed: faulty gap identifier' + part)
                sys.exit(1)
        else:
            accession = part.partition(":")[0].partition(".")[0].rpartition("complement(")[2]
            sequence = contigseqdict[accession]
            if "complement(" in part:
                sequence = str(Seq(sequence, generic_dna).reverse_complement())
            if ":" in part and ".." in part:
                seqstart, seqend = part.partition(":")[2].replace(")","").replace("(","").split("..")
                if int(seqstart) > 0:
                    seqstart = int(seqstart) - 1
                sequence = sequence[int(seqstart) : int(seqend)]
            fullsequence += sequence
    #Add compiled sequence to seq_record
    SeqObject = Seq(fullsequence, generic_dna)
    seq_record.seq = SeqObject
    updated_seq_records.append(seq_record)
    return updated_seq_records


def process_wgs_master_scaffolds(seq_records):
    updated_seq_records = []
    for seq_record in seq_records:
        #Check if seq_record is a WGS master record or a supercontig record
        if not (seq_record.annotations.has_key('wgs_scafld') or seq_record.annotations.has_key('wgs') or seq_record.annotations.has_key('contig')):
            updated_seq_records.append(seq_record)
        else:
            if seq_record.annotations.has_key('wgs_scafld') or seq_record.annotations.has_key('wgs'):
                updated_seq_records.extend(fix_wgs_master_record(seq_record))
            elif seq_record.annotations.has_key('contig'):
                updated_seq_records.extend(fix_supercontig_record(seq_record))
    return updated_seq_records


def is_nucl_seq(sequence):
    if len(str(sequence).lower().replace("a","").replace("c","").replace("g","").replace("t","").replace("n","")) < 0.2 * len(sequence):
        return True
    else:
        return False


def generate_nucl_seq_record(sequences):
    "Generate nucleotide seq_record"
    if len(sequences) == 0:
        return []
    seq_record = SeqRecord(Seq(""),id="Protein_Input", name="ProteinInput",
                   description="antiSMASH protein input")
    position = 0
    cds_features = []
    cdsnames = []
    for sequence in sequences:
        startpos = position
        endpos = position + len(sequence) * 3
        position += len(sequence) * 3 + 1000
        location = FeatureLocation(startpos, endpos)
        cdsfeature = SeqFeature(location, type="CDS")
        cdsfeature.strand = 1
        sequence_id = sequence.id[:15].replace(" ","_")
        if sequence_id not in cdsnames:
            cdsfeature.qualifiers['product'] = [sequence_id]
            cdsfeature.qualifiers['locus_tag'] = [sequence_id]
            cdsnames.append(sequence_id)
        else:
            x = 1
            while sequence_id[:8] + "_" + str(x) in cdsnames:
                x += 1
            cdsfeature.qualifiers['product'] = [sequence_id[:8] + "_" + str(x)]
            cdsfeature.qualifiers['locus_tag'] = [sequence_id[:8] + "_" + str(x)]
            cdsnames.append(sequence_id[:8] + "_" + str(x))
        cdsfeature.qualifiers['translation'] = [str(sequence.seq).replace('.', 'X')]
        cds_features.append(cdsfeature)
    seq_record.features.extend(cds_features)
    return [seq_record]


def add_translations(seq_records):
    "Add a translation qualifier to all CDS features"
    for seq_record in seq_records:
        cdsfeatures = utils.get_cds_features(seq_record)
        for cdsfeature in cdsfeatures:
            if not 'translation' in cdsfeature.qualifiers or len(cdsfeature.qualifiers['translation']) == 0:
                if len(seq_record.seq) == 0:
                    logging.error('No amino acid sequence in input entry for CDS %r, and no nucleotide sequence provided to translate it from.' % cdsfeature.id)
                    sys.exit(1)
                else:
                    import Bio
                    try:
                        translation = str(utils.get_aa_translation(seq_record, cdsfeature))
                    except Bio.Data.CodonTable.TranslationError, e:
                        logging.error('Getting amino acid sequences from CDS %r failed: %s' % \
                                (cdsfeature.id, e))
                        sys.exit(1)
                    cdsfeature.qualifiers['translation'] = [translation]


def add_seq_record_seq(seq_records):
    for seq_record in seq_records:
        if len(seq_record.seq) == 0:
            seqmax = max([cds.location.start for cds in utils.get_cds_features(seq_record)] + [cds.location.end for cds in utils.get_cds_features(seq_record)])
            seq_record.seq = Seq(seqmax * "n")


def check_duplicate_gene_ids(sequences):
    "Fix duplicate locus tags so that they are different"
    NO_TAG = "no_tag_found"
    high_water_mark = 0
    all_ids = defaultdict(lambda: False)
    for sequence in sequences:
        seq_ids = utils.get_cds_features(sequence)
        for cdsfeature in seq_ids:
            gene_id = utils.get_gene_id(cdsfeature)
            if not all_ids[gene_id]:
                all_ids[gene_id] = True
            else:
                if gene_id == NO_TAG:
                    x = high_water_mark + 1
                else:
                    x = 1
                id_str = "%s_%s" % ( gene_id[:8], x)
                while all_ids[id_str]:
                    x += 1
                    id_str = "%s_%s" % ( gene_id[:8], x)
                logging.debug("generated id %r" % id_str)
                cdsfeature.qualifiers['product'] = [id_str]
                cdsfeature.qualifiers['locus_tag'] = [id_str]
                all_ids[id_str] = True
                if gene_id == NO_TAG:
                    high_water_mark = x


def fix_id_lines(filename):
    infile = open(filename, "r")
    file_content = infile.read()
    infile.close()
    while file_content[:2] == "\n\n":
        file_content = file_content[1:]
    try:
        filetype = seqio._get_seqtype_from_ext(filename)
    except:
        return
    if filetype == "fasta":
        return
    elif filetype == "genbank":
        if "LOCUS       " not in file_content.partition("\n")[0]:
            file_content = "LOCUS       A01                    0 bp        DNA              BCT 01-JAN-2000\n" + file_content
    elif filetype == "embl":
        if "ID   " not in file_content.partition("\n")[0]:
            file_content = "ID   A01; SV 1; linear; unassigned DNA; STD; PRO; 0 BP.\nXX\n" + file_content
    outfile = open(filename,"w")
    outfile.write(file_content)
    outfile.close()


def parse_input_sequences(options):
    "Parse the input sequences from given filename"
    filenames = options.sequences
    logging.info('Parsing input sequences %r' % filenames)

    sequences = []
    for filename in filenames:

        if not path.exists(filename):
            logging.error('No sequence file found at %r' % filename)
            sys.exit(1)

        infile = open(filename, "r")
        file_content = infile.read()
        infile.close()
        if "Resource temporarily unavailable" in file_content[:200] or "<h1>Server Error</h1>" in file_content[:500] or "NCBI - WWW Error" in file_content[:500]:
            logging.error('ERROR: NCBI server temporarily unavailable: downloading ' + os.path.basename(filename) + ' failed.')
            sys.exit(1)

        fix_id_lines(filename)

        try:
            sequences.extend(list(seqio.parse(filename)))
        except ValueError, e:
            logging.error('Parsing %r failed: %s' % (filename, e))
            sys.exit(1)
        except AssertionError, e:
            logging.error('Parsing %r failed: %s' % (filename, e))
            sys.exit(1)
        except Exception, e:
            logging.error('Parsing %r failed with unhandled exception: %s' % \
                          (filename, e))
            sys.exit(1)
    #Check if seq_records have appropriate content
    i = 0
    while i < len(sequences):
        sequence = sequences[i]
        #Check if seq_record has either a sequence or has at least 80% of CDS features with 'translation' qualifier
        cdsfeatures = utils.get_cds_features(sequence)
        cdsfeatures_with_translations = [cdsfeature for cdsfeature in cdsfeatures if 'translation' in cdsfeature.qualifiers]
        if len(sequence.seq) == 0 or (options.input_type == 'nucl' and len(str(sequence.seq).replace("N","")) == 0 and len(cdsfeatures_with_translations) < 0.8 * len(cdsfeatures)):
            logging.error("Record %s has no sequence, skipping." % sequence.id)
            sequences.remove(sequence)
            continue

        if options.input_type == 'prot':
            if is_nucl_seq(sequence.seq):
                logging.error("Record %s is a nucleotide record, skipping." % sequence.id)
                sequences.remove(sequence)
                continue
        elif options.input_type == 'nucl':
            if not isinstance(sequence.seq.alphabet, NucleotideAlphabet) and not is_nucl_seq(sequence.seq):
                logging.error("Record %s is a protein record, skipping." % sequence.id)
                sequences.remove(sequence)
                continue
            if sequence.seq.alphabet != generic_dna:
                sequence.seq.alphabet = generic_dna

        i += 1

    #If protein input, convert all protein seq_records to one nucleotide seq_record
    if options.input_type == 'prot':
        sequences = generate_nucl_seq_record(sequences)

    #Handle WGS master or supercontig entries
    sequences = process_wgs_master_scaffolds(sequences)

    #Check if no duplicate locus tags / gene IDs are found
    check_duplicate_gene_ids(sequences)

    #If no CDS entries in records, run gene finding
    options.record_idx = 1
    i = 0
    while i < len(sequences):
        sequence = sequences[i]
        #Fix sequence name (will be ID) if it contains illegal chars
        illegal_chars  = '''!"#$%&()*+,:; \r\n\t=>?@[]^`'{|}/ '''
        for char in sequence.name:
            if char in illegal_chars:
                sequence.name = sequence.name.replace(char, "_")
        #Iterate through sequence objects
        if len(utils.get_cds_features(sequence)) < 1:
            logging.info("No CDS features found in record %r, running gene finding." % sequence.id)
            genefinding.find_genes(sequence, options)
            if len(utils.get_cds_features(sequence)) < 1:
                logging.info("No genes found, skipping record")
                sequences.remove(sequence)
                continue
        #Fix locus tags
        utils.fix_locus_tags(sequence, options)
        options.record_idx += 1
        i += 1

    #Make sure that all CDS entries in all seq_records have translation tags, otherwise add them
    add_translations(sequences)

    #Make sure that all seq_records have a sequence
    add_seq_record_seq(sequences)

    if len(sequences) > 1:
        options.start = -1
        options.end = -1
        logging.info("Discarding --from and --to options, as multiple entries are used.")

    i = 0
    while i < len(sequences):
        sequence = sequences[i]

        if options.start > 1:
            if options.start > len(sequence):
                logging.error('Specified analysis start point is at %r, which is larger ' \
                              'than record size %r' % (options.start, len(sequence)))
                sys.exit(1)
            sequence = sequence[options.start-1:]
            # new sequence is shorter, so fix the end calculation
            options.end -= options.start
            sequences[i] = sequence

        if options.end > 0:
            if options.end > len(sequence):
                logging.error('Specified analysis end point is at %r, which is larger ' \
                              'than record size %r' % (options.end, len(sequence)))
                sys.exit(1)
            sequence = sequence[:options.end]
            sequences[i] = sequence

        # Some programs write gaps as - not N, but translate() hates that
        if sequence.seq.find('-') > -1:
            sequence.seq = Seq(str(sequence.seq).replace('-', 'N'),
                               alphabet=sequence.seq.alphabet)

        # Some programs like to write gaps as X, translate() hates that
        if sequence.seq.find('X') > -1:
            sequence.seq = Seq(str(sequence.seq).replace('X', 'N'),
                               alphabet=sequence.seq.alphabet)
        if sequence.seq.find('x') > -1:
            sequence.seq = Seq(str(sequence.seq).replace('x', 'N'),
                               alphabet=sequence.seq.alphabet)

        i += 1

    return sequences


def check_prereqs(plugins, options):
    failure_messages = []
    failure_messages.extend(generic_modules.check_prereqs(options))
    for plugin in plugins:
        if 'check_prereqs' in dir(plugin):
            failure_messages.extend(plugin.check_prereqs())

    for msg in failure_messages:
        logging.error(msg)

    return len(failure_messages)


def detect_signature_genes(seq_record, clustertypes, options):
    "Detect different secondary metabolite clusters based on HMM signatures"
    logging.info('Looking for secondary metabolite cluster signatures')
    hmm_detection.detect_signature_genes(seq_record, clustertypes, options)


def detect_geneclusters(seq_record, options):
    if options.input_type == 'nucl':
        utils.log_status("Detecting secondary metabolite signature genes " \
                         "for contig #%d" % options.record_idx)
    else:
        utils.log_status("Detecting secondary metabolite signature genes " \
                         "for supplied amino acid sequences")
    detect_signature_genes(seq_record, options.enabled_cluster_types, options)
    if options.inclusive:
        utils.log_status("Detecting secondary metabolite clusters using "\
                         "inclusive algorithm for contig #%d" % options.record_idx)
        fullhmmer.run(seq_record, options)
        clusterfinder.run_cluster_finder(seq_record, options)
        options.fullhmmer = False


def cluster_specific_analysis(plugins, seq_record, options):
    "Run specific analysis steps for every detected gene cluster"
    logging.info('Running cluster-specific analyses')

    for plugin in plugins:
        if 'specific_analysis' in dir(plugin):
            logging.debug('Running analyses specific to %s clusters' %
                          plugin.short_description)
            plugin.specific_analysis(seq_record, options)
        else:
            logging.debug('No specific analyses implemented for %s clusters' %
                          plugin.short_description)


def unspecific_analysis(seq_record, config):
    "Run analyses independent of specific clusters"
    logging.info('Running analyses independent of specific cluster types')

    if config.full_hmmer:
        utils.log_status("Running full-genome PFAM analysis for contig #%d" % \
                         config.record_idx)
        fullhmmer.run(seq_record, config)
    # TODO: offer fullblast here


if __name__ == "__main__":
    main()
